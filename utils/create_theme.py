#!/usr/bin/python
# coding: utf-8

# Creates a theme file "theme.zip" to upload into the app directory 
# /storage/emulated/0/Android/data/org.catness.classroom/files/Documents/schooldata/

# Uses all the files with extensions gif,png and jpg in the current directory

# The files are sorted alphabetically!
# Update the theme.json manually and re-zip it, if you want to change the sorting


import os, sys, json
reload(sys)                      #reload is required, otherwise the script crashes
sys.setdefaultencoding('utf8')   #to deal with weird characters in some names
from glob import glob
from zipfile import ZipFile 

theme = {}

if len(sys.argv) < 2:
	print "Please specify the theme name !"
	sys.exit(0)
theme["name"] = sys.argv[1].strip()
theme["levels"] = []

files = glob('*.gif')
files.extend(glob('*.png'))
files.extend(glob('*.jpg'))

for file in sorted(files):
	name = file.rsplit('.',1)[0]
	level = {"name":name,"image":file}
	theme["levels"].append(level)

with open("theme.json","w") as f:
	 f.write(json.dumps(theme,ensure_ascii=False,indent=4))
	 f.write("\n")

with ZipFile('theme.zip','w') as zip: 
        for file in files: 
            zip.write(file) 
        zip.write("theme.json")    

print "Done."


