# correspondence between the filenames from the image set and the country names from the csv

russia : Russian Federation
united-states-of-america : United States
east-timor : Timor-Leste
north-korea : Korea, Dem. People's Rep.
egypt : Egypt, Arab Rep.
hong-kong : Hong Kong SAR, China
bahamas : Bahamas, The
macao : Macao SAR, China
yemen : Yemen, Rep.
south-korea : Korea, Rep.
st-lucia : St. Lucia
micronesia : Micronesia, Fed. Sts.
slovakia : Slovak Republic
gambia : Gambia, The
syria : Syrian Arab Republic
democratic-republic-of-congo : Congo, Dem. Rep.
republic-of-the-congo : Congo, Rep.
kyrgyzstan : Kyrgyz Republic
venezuela : Venezuela, RB
iran : Iran, Islamic Rep.
turks-and-caicos : Turks and Caicos Islands
marshall-island : Marshall Islands
st-vincent-and-the-grenadines : St. Vincent and the Grenadines
saint-kitts-and-nevis : St. Kitts and Nevis
brunei : Brunei Darussalam
virgin-islands : Virgin Islands (U.S.)
northern-marianas-islands : Northern Mariana Islands
