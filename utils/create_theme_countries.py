#!/usr/bin/python
# coding: utf-8

# resize : once
# for i in *.png; do echo "$i"; convert "$i" -scale 64x "$i"; done
# 
# after file resize, rename some files:
#
# mv 220-uzbekistn.png 220-uzbekistan.png 
# mv 191-tubalu.png 191-tuvalu.png
# mv 215-northen-cyprus.png 215-northern-cyprus.png
# mv 213-st-barts.png 213-saint-barthelemy.png
# mv 224-otan.png 224-nato.png

# (we use the filenames as the base names, so have to fix the wrong ones)

# population data set : API_SP.POP.TOTL_DS2_en_csv_v2_10576638.zip
# images : country flags set 555416-rectangular-country-simple-flags.zip 
# by Freepik : https://www.freepik.com from https://www.flaticon.com/


import os, sys, json, csv, collections, string
reload(sys)                  
sys.setdefaultencoding('utf8')
from glob import glob
from zipfile import ZipFile

def convert(name):
	name = name.replace('-',' ')
	name = string.capwords(name)
	return name

# countries in csv but with different names than the filenames
translations = {}
with open('translations.txt', 'r') as f:
	for line in f:
		if line[0] == '#': continue
		line = line.strip().lower()
		if ':' not in line: continue
		(filename,csvname) = line.split(' : ')
		translations[csvname] = filename

# countries which are not in csv (population is taken from wiki)
additions = {}
with open('additions.txt', 'r') as f:
	for line in f:
		if line[0] == '#': continue
		line = line.strip()
		if ':' not in line: continue
		(name,pop) = line.split(' : ')
		pop = pop.replace(',','')
		additions[name] = int(pop)



countries = {}

with open('pop.csv', 'rb') as csvfile:
	reader = csv.reader(csvfile, delimiter=',')
	for row in reader:
		if len(row) < 4: continue
		country = row[0]
		for i in range(3,len(row)-3):
			population = row[-i]
			if population != "": break
		#print country + " : " + population
		if population == "": continue

		country = country.lower()
		if country in translations:
			country = translations[country]
		else:	
			country = country.replace(' ','-')
		countries[country] = int(population)

#print len(countries)
#countries_sorted = sorted(countries.items(), key=lambda x: x[1], reverse=True)
#for name,pop in countries_sorted:
#	print name + " : " + str(pop)

names = {}
filenames = {}
files = glob('*.png')
for file in files:
	name = file.rsplit('.',1)[0]
	name = name[4:]
	filenames[name] = file
	if name in countries:
		names[name] = countries[name]
	elif name in additions:
		names[name] = additions[name]
	else:	
		names[name] = 0


names_sorted = sorted(names.items(), key=lambda x: x[1], reverse=True)

for name,pop in names_sorted:
	title = convert(name)
	print name + " : " + title + " : " + str(pop)


theme = {}
theme["name"] = "Countries"
theme["levels"] = []

names_sorted = sorted(names.items(), key=lambda x: x[1])
for name,pop in names_sorted:
	title = convert(name)
	file = filenames[name]
	level = {"name":title,"image":file}
	theme["levels"].append(level)

with open("theme.json","w") as f:
	f.write(json.dumps(theme,ensure_ascii=False,indent=4))
	f.write("\n")

with ZipFile('theme.zip','w') as zip:
	for file in files:
		zip.write(file)
	zip.write("theme.json")






