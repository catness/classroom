# Done

[x] Status : table, DAO, layout etc. Only in main activity.
   (in the table: level, points)

[x] Update status.points (and level when applicable) when adding /deleting reports

[x] Delete report

[x] Update status points when updating reports (must keep and send the old points value)

[x] Delete subject

[x] Select subjects from the main activity - menu (so it's possible to add/edit without adding a new report)

[x] Level titles as Tarot cards 

[x] Reset the status and reports for debugging

[x] Improve the status bar: add a Tarot card (Add all the Tarot deck images to the app)

[x] Update fonts and colors for the status bar

[x] Color wheel instead of hex color

[x] Replace the floppy "save" icon with a check mark

[x] Add all the Tarot images

[x] Subjects position, with an option to move up/down

[x] Verify date/time

[x] Verify points (integer)

[x] Paging of reports

[x] Rewards table (DAO etc) and an activity to add/update/edit it (from the main menu)

[x] DB migration which keeps the class list

[x] Display a random reward upon level up (and update its amount in the rewards table). 

[x] Refactor the issue with the public ReportDao in data repository

[x] Auto calculate the text color for subjects and reports (dark on light backgrounds and vice versa)

[x] Custom layout for level up popup

[x] Update "new reward" layout with comments for field names

[x] Show the weight and amount of rewards in the list of selections

[x] Add a nice icon

[x] Calendar view with "heatmap" - colors corresponding to the number of points

[x] Add sounds (ding) when getting/removing points, and on level up

[x] A new activity to show reports for one day (called by clicking on a date), together with the comments.

[x] Add new report under report-for-one-day activity, with the date preset to that date (fix the bug with the missing subject)

[x] Implement editing reports in report-for-one-day activity 

[x] Refactor MainActivity and ReportsDayActivity to remove duplicate code

[x] Check for level up when editing reports (because points may be changed!)

[x] Add settings activity with the option to mute/unmute the sound

[x] Add separators between different dates on reports main page

[x] Move the shared preferences interface to the main activity (from the common report code)

[x] Add "debug" setting to show/hide reset item in the main menu 

[x] Show the number of today's points on the main page

[x] Backup to json

[x] Setting how to export data : file or email

[x] Customise the toolbar title in preferences

[x] Import data 

[x] Change "classroom" string to constant and make it "schooldata" like in SchoolDatabase

[x] Make export an async task, and load subjects there, do not use livedata in the main activity

[x] Change the levelup frame to use royalty free graphics (ornamental corners)

[x] Remove export to internal storage because it's useless

[x] Dark theme; switch between dark and light themes in preferences

[x] Dark theme for the menu

[x] Dark/light theme for the date headers

[x] Dark theme for the icon in General preferences

[x] Dark theme for the calendar - change date background and number colors

[x] Settings night mode: text input widget has wrong colors (when changing app title)

[x] CalendarActivity: process invalid date exception in onPostExecute (CalendarDay.from)

[x] Dark/light theme for the levelup message

[x] A setting to export to SD card if available

[x] Check if files are actually deleted from cache

[x] Bug: Calendar view -> update report -> cancel : returns to the main page, not back to the calendar view

[x] When the amount of reward is 0, change the color in the dark mode from white

[x] Calendar day report view swipe to prev/next day

[x] Refactor the MainActivity and ReportsDayActivity - make a common parent class with the common code

[x] Make the preference for the number of reports on the main page

[x] Fix the export (add a function to get really all reports)

[x] Add time picker for new/edit report

[x] Swipe on calendar days with 0 points ("No reports") 

[x] Make the export fully async

[x] Add 1 report, 1 reward and a few subjects to init of an empty database

[x] Fix a bug with rewards greyout upon adding a new one

[x] Fix color picker in the dark mode

[x] Drag&drop subjects without stopping after every swap

[x] Refactor the sources into different directories

[x] Fix "navigate up" in the calendar to return to the same month

[x] Fix the lint warnings  - ./gradlew lint  (and code inspection with right click -> analyze -> inspect code)

[x] New database tables / entities : level_theme (id, name, maxpoints, increment) and levels (id, level(id), name, pos, image)

[x] DB migration 

[x] Upload the whole theme as a json/zip? file

[x] Show the error message if the theme with the same name already exists

[x] Create another path for images if the path already exists

[x] deleteThemeAsyncTask in repository: delete also the theme levels, and the image files (and the directory)

[x] A new preference option - active theme (but not in the settings menu)

[x] Show the active theme (label "active?") in the theme adapter

[x] The checkbox in the theme edit menu to make this theme active

[x] Use the images and names from the active theme

[x] Upon deleting the theme, change the active theme to the default 

[x] update the theme info in levelShow not in onPostResume, but use "activity for result" for themes, and update it on return

[x] Fix the dark mode for theme edit

[x] Check the theme for missing files and add to the comment

[x] Change the Reset option to reset only points, not reports

[x] Fix today's points (10) not showing upon 1st installation

[x] Interface for changing the MAXPOINTS  (in preferences)

[x] Create the animals theme

[x] Create the flags theme (by population)

[x] Code cleanup

[x] A setting to show/hide the Themes option in the main menu

# Maybe

* Quests: select a random subject and require to get a random amount of points on it (20-100?)



