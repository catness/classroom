* Sound clips : https://www.zapsplat.com
* Tarot cards : https://mozz.itch.io/the-arcade-arcanum
* Launcher icon: https://www.flaticon.com/authors/smalllikeart
* 9-patch border : https://all-free-download.com/free-vector/download/corner_ornament_vintage_vector_574761_download.html
* Animals icon set (for animals level theme) by Icons8: https://icons8.com/
* Country flags set (for country flags theme) by Freepik : https://www.freepik.com from https://www.flaticon.com/

