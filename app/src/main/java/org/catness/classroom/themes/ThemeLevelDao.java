package org.catness.classroom.themes;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ThemeLevelDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(ThemeLevel themeLevel);

    @Query("select * from themelevels where theme=:id order by level")
    List<ThemeLevel> getAllThemeLevelsSync(int id);

    @Query("select count(*) from themelevels where theme=:theme")
    int getCount(int theme);

    @Query("DELETE FROM themelevels where theme=:theme")
    void deleteAll(int theme);

    @Query("select * from themelevels where theme=:id and level=:level")
    ThemeLevel getThemeLevel(int id, int level);

}
