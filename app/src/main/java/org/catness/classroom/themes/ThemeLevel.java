package org.catness.classroom.themes;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import static org.catness.classroom.utils.Utils.str;

@Entity(tableName = "themelevels")
public class ThemeLevel {
    private final static String LOG_TAG = ThemeLevel.class.getSimpleName();
    @PrimaryKey(autoGenerate = true)
    public int id;

    @NonNull
    public String name;
    public int theme; // a key into themes
    public int level; // position - sequential from 1 (or 0?) without skipping
    public String image; // image path

    public ThemeLevel(@NonNull String name, int theme, int level, String image) {
        this.name = name;
        this.theme = theme;
        this.level = level;
        this.image = image;
    }

    @Ignore
    public ThemeLevel(int id, @NonNull String name, int theme, int level, String image) {
        this.id = id;
        this.name = name;
        this.theme = theme;
        this.level = level;
        this.image = image;
    }

    @Ignore
    public ThemeLevel() {
    }

    public String toString() {
        return "ThemeLevel: " + name + " : theme=" + str(theme) + " : level=" + str(level) + " : " + image;
    }


}
