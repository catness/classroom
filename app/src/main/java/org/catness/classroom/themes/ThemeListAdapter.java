package org.catness.classroom.themes;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.catness.classroom.R;

import java.util.List;

import static org.catness.classroom.utils.Utils.str;

public class ThemeListAdapter extends RecyclerView.Adapter<ThemeListAdapter.ThemeViewHolder> {
    private final static String LOG_TAG = ThemeListAdapter.class.getSimpleName();
    private final LayoutInflater mInflater;
    private List<Theme> themes;
    private static ClickListener clickListener;
    private int selectedTheme;

    public ThemeListAdapter(Context context, int selectedTheme) {
        mInflater = LayoutInflater.from(context);
        this.selectedTheme = selectedTheme;

        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.windowBackground, typedValue, true)) {
            int bgColor = typedValue.data;
            Log.d(LOG_TAG, "bgColor = " + str(bgColor));
        }
    }


    @NonNull
    @Override
    public ThemeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = mInflater.inflate(R.layout.theme_item, parent, false);
        return new ThemeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ThemeViewHolder holder, int position) {
        if (themes != null) {
            Theme current = themes.get(position);
            Log.d(LOG_TAG, "theme: " + current.toString());
            holder.nameView.setText(current.name);
            holder.numberView.setText(str(current.count));
            if (current.id == selectedTheme) {
                holder.activeView.setVisibility(View.VISIBLE);
            } else {
                holder.activeView.setVisibility(View.GONE);
            }
        } else {
            // Covers the case of data not being ready yet.
            //holder.nameView.setText("No rewards yet");
            Log.d(LOG_TAG, "no themes yet");
        }
    }

    public void setThemes(List<Theme> params) {
        themes = params;
        notifyDataSetChanged();
    }

    public void setSelectedTheme(int themeID) {
        selectedTheme = themeID;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (themes != null)
            return themes.size();
        else return 0;
    }

    public Theme getThemeAtPosition(int position) {
        return themes.get(position);
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        ThemeListAdapter.clickListener = clickListener;
    }

    public class ThemeViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameView, numberView;
        private final ImageView activeView;

        ThemeViewHolder(@NonNull View itemView) {
            super(itemView);
            nameView = itemView.findViewById(R.id.nameView);
            numberView = itemView.findViewById(R.id.numberView);
            activeView = itemView.findViewById(R.id.activeSign);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onItemClick(view, getAdapterPosition());
                }
            });

        }
    }

}
