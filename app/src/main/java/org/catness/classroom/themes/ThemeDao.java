package org.catness.classroom.themes;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface ThemeDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Theme theme);

    @Query("select * from themes order by name")
    LiveData<List<Theme>> getAllThemes();

    @Query("select * from themes where id=:id")
    Theme get(int id);

    @Query("delete from themes where id=:id")
    void delete(int id);

    @Update
    void update(Theme... theme);

    @Query("select count(*) from themes")
    int getCount();

    @Query("select id from themes where name=:name")
    int getId(String name);

    @Query("select path from themes where id=:id")
    String getPath(int id);


}


