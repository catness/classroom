package org.catness.classroom.themes;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import org.catness.classroom.database.DataRepository;

import java.util.List;

public class ThemeViewModel extends AndroidViewModel {
    private final static String LOG_TAG = ThemeViewModel.class.getSimpleName();
    private final DataRepository repository;
    private final LiveData<List<Theme>> themes;

    public ThemeViewModel(@NonNull Application application) {
        super(application);
        repository = new DataRepository(application);
        themes = repository.getAllThemes();
    }

    public LiveData<List<Theme>> getAllThemes() {
        return themes;
    }

    public void update(Theme theme) {
        repository.updateTheme(theme);
    }

}
