package org.catness.classroom.themes;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import static org.catness.classroom.utils.Utils.str;

@Entity(tableName = "themes")
public class Theme {
    private final static String LOG_TAG = Theme.class.getSimpleName();
    @PrimaryKey(autoGenerate = true)
    public int id;

    @NonNull
    public String name;
    @NonNull
    public String path; // filepath for images
    public int count; // number of levels
    public String comment;

    public Theme(@NonNull String name, @NonNull String path, int count, String comment) {
        this.name = name;
        this.path = path;
        this.count = count;
        this.comment = comment;
    }

    @Ignore
    public Theme(int id, @NonNull String name, @NonNull String path, int count, String comment) {
        this.id = id;
        this.name = name;
        this.path = path;
        this.count = count;
        this.comment = comment;
    }

    @Ignore
    public Theme() {
    }

    public String toString() {
        return "Theme: " + str(id) + " : " + name + " : " + path + " : " + str(count) + " : " + comment;
    }
}
