package org.catness.classroom.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.style.ForegroundColorSpan;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import org.catness.classroom.R;

import static org.catness.classroom.utils.Utils.getContrastColor;

// a decorator for 1 day only ("day" in the constructor)
public class CurrentDayDecorator implements DayViewDecorator {

    private final int color;
    private final Context context;
    private final CalendarDay day;

    public CurrentDayDecorator(int color, CalendarDay day, Context context) {
        this.color = color;
        this.context = context;
        this.day = day;
    }

    @Override
    public boolean shouldDecorate(CalendarDay current) {
        return (current.equals(day));
    }

    @Override
    public void decorate(DayViewFacade view) {
        Drawable drawable = context.getResources().getDrawable(R.drawable.customday);
        drawable = drawable.mutate();
        drawable.setTint(color);
        view.setBackgroundDrawable(drawable);
        view.addSpan(new ForegroundColorSpan(getContrastColor(color)));
    }
}