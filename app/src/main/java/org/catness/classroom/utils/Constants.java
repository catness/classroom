package org.catness.classroom.utils;

import android.graphics.Color;
import android.support.v7.widget.helper.ItemTouchHelper;

import java.util.LinkedHashMap;
import java.util.Map;

public class Constants {
    public static final int NEW_REPORT_ACTIVITY_REQUEST_CODE = 1;
    public static final int UPDATE_REPORT_ACTIVITY_REQUEST_CODE = 2;
    public static final int NEW_SUBJECT_ACTIVITY_REQUEST_CODE = 3;
    public static final int UPDATE_SUBJECT_ACTIVITY_REQUEST_CODE = 4;
    public static final int NEW_REWARD_ACTIVITY_REQUEST_CODE = 5;
    public static final int UPDATE_REWARD_ACTIVITY_REQUEST_CODE = 6;
    public static final int CALENDAR_ACTIVITY_REQUEST_CODE = 7;
    public static final int UPDATE_THEME_ACTIVITY_REQUEST_CODE = 8;
    public static final int SELECT_THEME_REQUEST_CODE = 9;

    public static final int MAXPOINTS = 1000;

    public static final String ID = "id";
    public static final String SUBJECT = "subject";
    public static final String COLOR = "color";
    public static final String POINTS = "points";
    public static final String LASTPOINTS = "lastpoints";
    public static final String DATETIME = "datetime";
    public static final String COMMENT = "comment";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String DELETE = "delete";
    public static final String REPORTS = "reports";
    public static final String POS = "pos";
    public static final String NAME = "name";
    public static final String WEIGHT = "weight";
    public static final String AMOUNT = "amount";
    public static final String DAY = "day";
    public static final String MONTH = "month";
    public static final String YEAR = "year";
    public static final String COUNT = "count";
    public static final String PATH = "path";
    public static final String ACTIVE = "active";

    public static final int TOUCH_DRAG = 1;
    public static final int TOUCH_SWIPE = 2;

    public static final int SWIPE_LEFT = ItemTouchHelper.START;
    public static final int SWIPE_RIGHT = ItemTouchHelper.END;

    public static final int POS_TMP = -9; // for swapping subjects; must be <0 so it comes first on the adapter data

    public static final String PREF_SOUND = "pref_sound";
    public static final String PREF_RESET = "pref_reset";
    public static final String PREF_EXPORT = "pref_export";
    public static final String PREF_IMPORT_SHOW = "pref_import_show";
    public static final String PREF_EXPORT_SHOW = "pref_export_show";
    public static final String PREF_APP_HEADER = "pref_app_header";
    public static final String PREF_NIGHT = "pref_night";
    public static final String PREF_LIMIT = "pref_limit";
    public static final String PREF_THEME = "pref_theme";
    public static final String PREF_THEME_SHOW = "pref_theme_show";
    public static final String PREF_MAXPOINTS = "pref_maxpoints";
    public static final int REPORTS_LIMIT = 100;

    public static final int MENU_RESET = 999;
    public static final int MENU_EXPORT = 950;
    public static final int MENU_IMPORT = 960;
    public static final int MENU_THEME = 970;

    public static final int EXPORT_EXTERNAL = 0;
    public static final int EXPORT_SDCARD = 1;
    public static final int EXPORT_EMAIL = 2;
    public static final int EXPORT_INTERNAL = 3;

    public static final String IMPORT_FILENAME = "db_import.json";
    public static final String IMPORT_FILENAME_SAVE = "db_import.json.save";
    public static final String DATABASE_NAME = "schooldata";
    public static final String THEME_FILENAME = "theme.zip";
    public static final int THEME_DEFAULT = 1;

    // from https://riptutorial.com/r/example/28354/colorblind-friendly-palettes (BuGn)  (almost)
    public static final Map<Integer, Integer> paletteColors;

    static {
        paletteColors = new LinkedHashMap<>();
        // if points <= key, color = value
        paletteColors.put(0, Color.parseColor("#f7fcfd"));
        paletteColors.put(15, Color.parseColor("#e5f5f9"));
        paletteColors.put(29, Color.parseColor("#ccece6"));
        paletteColors.put(44, Color.parseColor("#99d8c9"));
        paletteColors.put(59, Color.parseColor("#66c2a4"));
        paletteColors.put(73, Color.parseColor("#41ae76"));
        paletteColors.put(88, Color.parseColor("#238b45"));
        paletteColors.put(100, Color.parseColor("#006d2c"));
        paletteColors.put(Integer.MAX_VALUE, Color.parseColor("#006d2c"));
    }

}
