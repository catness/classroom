package org.catness.classroom.utils;

import android.graphics.Canvas;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;

import org.catness.classroom.subjects.SubjectListAdapter;

import static org.catness.classroom.utils.Constants.TOUCH_DRAG;
import static org.catness.classroom.utils.Constants.TOUCH_SWIPE;
import static org.catness.classroom.utils.Utils.str;

public class SimpleItemTouchHelperCallback extends ItemTouchHelper.Callback {
    private static final String LOG_TAG = SimpleItemTouchHelperCallback.class.getSimpleName();
    private final ItemTouchHelperAdapter mAdapter;
    private final int mode;
    private int dragFrom = -1, dragTo = -1;

    public SimpleItemTouchHelperCallback(ItemTouchHelperAdapter adapter, int mode) {
        mAdapter = adapter;
        this.mode = mode;
        initDrag();
    }

    private void initDrag() {
        dragFrom = -1;
        dragTo = -1;
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return ((mode & TOUCH_DRAG) != 0);
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return ((mode & TOUCH_SWIPE) != 0);
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
        if ((mode & TOUCH_SWIPE) != 0) {
            Log.d(LOG_TAG, "direction=" + str(direction));
            //mAdapter.onItemDismiss(viewHolder.getAdapterPosition());
            mAdapter.onItemDismiss(direction);
        }
    }

    private void reallyMoved(int from, int to) {
        ((SubjectListAdapter) mAdapter).reallyMoved(from, to);
    }

    @Override
    public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
        int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        if ((mode & TOUCH_DRAG) != 0) {
            int fromPosition = viewHolder.getAdapterPosition();
            int toPosition = target.getAdapterPosition();

            if (dragFrom == -1) {
                dragFrom = fromPosition;
            }
            dragTo = toPosition;
            Log.d(LOG_TAG, "onMove: " + str(dragFrom) + " -> " + str(dragTo));

            mAdapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
            return true;
        }
        return false;
    }

    // https://stackoverflow.com/questions/35920584/android-how-to-catch-drop-action-of-itemtouchhelper-which-is-used-with-recycle
    @Override
    public void clearView(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        super.clearView(recyclerView, viewHolder);
        Log.d(LOG_TAG, "clearView: dragFrom=" + str(dragFrom) + " dragTo=" + str(dragTo));
        if (dragFrom != -1 && dragTo != -1 && dragFrom != dragTo) {
            reallyMoved(dragFrom, dragTo);
        }
        initDrag();
    }

    @Override
    public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        if ((mode & TOUCH_DRAG) != 0) {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    }

}