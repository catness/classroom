package org.catness.classroom.utils;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.util.Log;
import android.util.TypedValue;

import org.threeten.bp.LocalDateTime;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.Locale;

public class Utils {
    private static final String LOG_TAG = Utils.class.getSimpleName();
    // private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
    // SimpleDateFormat is not thread-safe!!! so it can't be static in the threaded environments.
    //private static final SimpleDateFormat formatShort = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
    //private static final SimpleDateFormat formatDateOnly = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    private static final DateTimeFormatter formatterShort = DateTimeFormatter.ofPattern("HH:mm");
    private static final DateTimeFormatter formatterDateOnly = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private static final ZoneId zoneId = ZoneId.systemDefault();
    private static final ZoneOffset zoneOffset = OffsetDateTime.now(ZoneId.systemDefault()).getOffset();

    // private static final Random randomGenerator = new Random();    // currently unused

    public static String str(int i) {
        return Integer.toString(i);
    }

    public static String str(float i) {
        return Float.toString(i);
    }

    public static String str(double i) {
        return Double.toString(i);
    }

    public static String str(boolean i) {
        return Boolean.toString(i);
    }

    public static String strDateTime(long time) {
        /*
        Date date = new Date(time * 1000);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        return format.format(date);
        */
        LocalDateTime dateTime = LocalDateTime.ofEpochSecond(time, 0, zoneOffset);
        String formattedDate = dateTime.format(formatter);
        return formattedDate;
    }

    public static String strDateTime() {
        /*
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        return format.format(date);
        */
        long ts = System.currentTimeMillis() / 1000;
        return strDateTime(ts);
    }

    public static String strDateTimeShort(long time) {
    /*
        Date date = new Date(time * 1000);
        return formatShort.format(date);
        */
        return LocalDateTime.ofEpochSecond(time, 0, zoneOffset).format(formatterShort);
    }

    public static String strDateOnly(long time) {
    /*
        Date date = new Date(time * 1000);
        return formatDateOnly.format(date);
    */
        return LocalDateTime.ofEpochSecond(time, 0, zoneOffset).format(formatterDateOnly);
    }

    public static String strDateToday() {
        /*
        Date date = new Date();
        return formatDateOnly.format(date);
        */
        long ts = System.currentTimeMillis() / 1000;
        return strDateOnly(ts);
    }

    public static Long dateToTimestamp(String datetime) {
        // http://bigdatums.net/2016/01/23/how-to-convert-timestamp-to-unix-time-epoch-in-java/
        if (datetime.equals("")) return System.currentTimeMillis() / 1000L; // current timestamp
        //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        try {
            //Date dt = format.parse(datetime);
            LocalDateTime dt = LocalDateTime.parse(datetime, formatter);
            long epoch = dt.atZone(zoneId).toEpochSecond();
            //long epoch = dt.getTime();
            //Long result = epoch / 1000;

           // Log.d(LOG_TAG, "~~~~~~~~~~ DATEToTimestamp : datetime=" + datetime + " date= " + dt.toString() + " epoch = " + str(epoch));
            return epoch /*  / 1000 */;
        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage());
            return 0L;
        }
    }

    public static Long startDay(int year, int month, int day) {
        String date = String.format(Locale.ENGLISH, "%04d-%02d-%02d 00:00", year, month, day);
        return dateToTimestamp(date);
    }

    public static Long endDay(int year, int month, int day) {
        String date = String.format(Locale.ENGLISH, "%04d-%02d-%02d 23:59", year, month, day);
        return dateToTimestamp(date);
    }

    /* Currently unused

    public static String colorHex(int color) {
        int a = Color.alpha(color);
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);
        return String.format(Locale.ENGLISH, "#%02X%02X%02X%02X", a, r, g, b);
    }

    public static String generateColor() {
        int newColor = randomGenerator.nextInt(0x1000000);
        return String.format(Locale.ENGLISH, "#%06X", newColor);
    }
    */

    // from https://stackoverflow.com/questions/1855884/determine-font-color-based-on-background-color
    @ColorInt
    public static int getContrastColor(@ColorInt int color) {
        // Counting the perceptive luminance - human eye favors green color...
        double a = 1 - (0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color)) / 255;

        int d;
        if (a < 0.5) {
            d = 0; // bright colors - black font
        } else {
            d = 255; // dark colors - white font
        }

        return Color.rgb(d, d, d);
    }


    public static int getStyleColor(Context context, String attr) {
        TypedValue value = new TypedValue();
        int colorAttr = context.getResources().getIdentifier(attr, "attr", context.getPackageName());
        context.getTheme().resolveAttribute(colorAttr, value, true);
        return value.data;
    }

}
