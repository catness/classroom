package org.catness.classroom;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.catness.classroom.rewards.Reward;
import org.catness.classroom.rewards.RewardListAdapter;
import org.catness.classroom.rewards.RewardViewModel;

import java.util.List;

import static org.catness.classroom.utils.Constants.AMOUNT;
import static org.catness.classroom.utils.Constants.DELETE;
import static org.catness.classroom.utils.Constants.DESCRIPTION;
import static org.catness.classroom.utils.Constants.ID;
import static org.catness.classroom.utils.Constants.NAME;
import static org.catness.classroom.utils.Constants.NEW_REWARD_ACTIVITY_REQUEST_CODE;
import static org.catness.classroom.utils.Constants.PREF_APP_HEADER;
import static org.catness.classroom.utils.Constants.UPDATE_REWARD_ACTIVITY_REQUEST_CODE;
import static org.catness.classroom.utils.Constants.WEIGHT;

public class RewardsSelectActivity extends AppCompatActivity {
    private static final String LOG_TAG = RewardsSelectActivity.class.getSimpleName();
    private RewardViewModel rewardViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (((App) getApplicationContext()).isNightModeEnabled())
            setTheme(R.style.ActivityThemeDark);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewards_select);
        initPreferences();

        RecyclerView recyclerView = findViewById(R.id.recyclerview_rewards);
        final RewardListAdapter adapter = new RewardListAdapter(this);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager l = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(l);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                l.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        rewardViewModel = ViewModelProviders.of(this).get(RewardViewModel.class);
        /*
        When the observed data changes while the activity is in the foreground,
        the onChanged() method is invoked and updates the data cached in the adapter.
        Note that in this case, when the app opens, the initial data is added, so onChanged() method is called.
        */
        rewardViewModel.getAllRewards().observe(this, new Observer<List<Reward>>() {
            @Override
            public void onChanged(@Nullable final List<Reward> rewards) {
                // Update the cached copy of the words in the adapter.
                adapter.setRewards(rewards);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RewardsSelectActivity.this, NewRewardActivity.class);
                Log.d(LOG_TAG, "new reward............");
                startActivityForResult(intent, NEW_REWARD_ACTIVITY_REQUEST_CODE);
            }
        });

        adapter.setOnItemClickListener(new RewardListAdapter.ClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Reward reward = adapter.getRewardAtPosition(position);
                launchUpdateRewardActivity(reward);
            }
        });

    }

    private void launchUpdateRewardActivity(Reward reward) {
        Intent intent = new Intent(this, NewRewardActivity.class);
        intent.putExtra(NAME, reward.name);
        intent.putExtra(DESCRIPTION, reward.description);
        intent.putExtra(ID, reward.id);
        intent.putExtra(WEIGHT, reward.weight);
        intent.putExtra(AMOUNT, reward.amount);
        startActivityForResult(intent, UPDATE_REWARD_ACTIVITY_REQUEST_CODE);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case NEW_REWARD_ACTIVITY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    String name = data.getStringExtra(NAME);
                    String description = data.getStringExtra(DESCRIPTION);
                    Integer weight = data.getIntExtra(WEIGHT, 1);
                    Integer amount = data.getIntExtra(AMOUNT, 1);
                    Reward reward = new Reward(name, description, weight, amount);
                    Log.d(LOG_TAG, "adding new reward: " + reward.toString());
                    rewardViewModel.insert(reward);
                } else {
                    Toast.makeText(getApplicationContext(), R.string.empty_not_saved, Toast.LENGTH_LONG).show();
                }
                break;
            case UPDATE_REWARD_ACTIVITY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    int id = data.getIntExtra(ID, -1);
                    if (data.getExtras().containsKey(DELETE)) {
                        rewardViewModel.delete(id);
                    } else {
                        String name = data.getStringExtra(NAME);
                        String description = data.getStringExtra(DESCRIPTION);
                        Integer weight = data.getIntExtra(WEIGHT, 1);
                        Integer amount = data.getIntExtra(AMOUNT, 1);
                        Reward reward = new Reward(id, name, description, weight, amount);
                        if (id != -1) {
                            Log.d(LOG_TAG, "updating new reward: " + reward.toString());
                            rewardViewModel.update(reward);
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.error_updating_reward, Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.empty_not_saved, Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void initPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        //sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        String appHeader = sharedPreferences.getString(PREF_APP_HEADER, getString(R.string.app_name));
        String header = this.getString(R.string.title_select_rewards, appHeader);
        setTitle(header);
    }
}
