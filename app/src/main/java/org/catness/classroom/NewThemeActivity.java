package org.catness.classroom;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.EditText;

import static org.catness.classroom.utils.Constants.ACTIVE;
import static org.catness.classroom.utils.Constants.COMMENT;
import static org.catness.classroom.utils.Constants.COUNT;
import static org.catness.classroom.utils.Constants.DELETE;
import static org.catness.classroom.utils.Constants.ID;
import static org.catness.classroom.utils.Constants.NAME;
import static org.catness.classroom.utils.Constants.PATH;
import static org.catness.classroom.utils.Constants.THEME_DEFAULT;

public class NewThemeActivity extends AppCompatActivity {
    private static final String LOG_TAG = NewThemeActivity.class.getSimpleName();
    private EditText editTitle, editComment;
    private CheckBox activeSign;
    private int themeID;
    private int count;
    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (((App) getApplicationContext()).isNightModeEnabled())
            setTheme(R.style.ActivityThemeDark);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_theme);

        editTitle = findViewById(R.id.titleEdit);
        editComment = findViewById(R.id.commentEdit);
        activeSign = findViewById(R.id.isActive);
        Bundle extras = getIntent().getExtras();
        themeID = extras.getInt(ID, -1);
        Log.d(LOG_TAG, "onCreate: themeID=" + themeID);
        count = extras.getInt(COUNT, 0);
        path = extras.getString(PATH);
        boolean isActive = extras.getBoolean(ACTIVE);
        String name = extras.getString(NAME, "");
        String comment = extras.getString(COMMENT, "");
        editTitle.setText(name);
        editComment.setText(comment);
        activeSign.setChecked(isActive);
        setTitle("Update theme");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_edit_theme, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        // If this is the initial hardcoded theme, hide the "Delete" menu item.
        if (themeID == THEME_DEFAULT) {
            MenuItem menuItem = menu.findItem(R.id.action_delete);
            menuItem.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.action_save:
                updateItem();
                finish();
                return true;
            // Respond to a click on the "Delete" menu option
            case R.id.action_delete:
                showDeleteConfirmationDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateItem() {
        Intent replyIntent = new Intent();
        if (TextUtils.isEmpty(editTitle.getText())) {
            setResult(RESULT_CANCELED, replyIntent);
        } else {
            replyIntent.putExtra(COUNT, count);
            replyIntent.putExtra(PATH, path);
            replyIntent.putExtra(NAME, editTitle.getText().toString());
            replyIntent.putExtra(COMMENT, editComment.getText().toString());
            replyIntent.putExtra(ACTIVE, activeSign.isChecked());
            replyIntent.putExtra(ID, themeID);
            // Set the result status to indicate success.
            setResult(RESULT_OK, replyIntent);
        }
    }

    private void showDeleteConfirmationDialog() {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the positive and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_theme);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Delete" button, so delete the pet.
                deleteItem();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Cancel" button, so dismiss the dialog
                // and continue editing the pet.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /**
     * Perform the deletion of the pet in the database.
     */
    private void deleteItem() {
        Intent replyIntent = new Intent();
        replyIntent.putExtra(ID, themeID);
        replyIntent.putExtra(DELETE, true);
        setResult(RESULT_OK, replyIntent);
        finish();
    }


}
