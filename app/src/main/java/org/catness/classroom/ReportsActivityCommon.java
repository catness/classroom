package org.catness.classroom;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.catness.classroom.levels.LevelShow;
import org.catness.classroom.reports.Report;
import org.catness.classroom.reports.ReportViewModel;
import org.catness.classroom.rewards.Reward;
import org.catness.classroom.rewards.RewardViewModel;
import org.catness.classroom.utils.Utils;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Locale;

import static org.catness.classroom.utils.Constants.COLOR;
import static org.catness.classroom.utils.Constants.COMMENT;
import static org.catness.classroom.utils.Constants.DATETIME;
import static org.catness.classroom.utils.Constants.DELETE;
import static org.catness.classroom.utils.Constants.ID;
import static org.catness.classroom.utils.Constants.LASTPOINTS;
import static org.catness.classroom.utils.Constants.MAXPOINTS;
import static org.catness.classroom.utils.Constants.NEW_REPORT_ACTIVITY_REQUEST_CODE;
import static org.catness.classroom.utils.Constants.POINTS;
import static org.catness.classroom.utils.Constants.SELECT_THEME_REQUEST_CODE;
import static org.catness.classroom.utils.Constants.SUBJECT;
import static org.catness.classroom.utils.Constants.UPDATE_REPORT_ACTIVITY_REQUEST_CODE;
import static org.catness.classroom.utils.Utils.endDay;
import static org.catness.classroom.utils.Utils.startDay;
import static org.catness.classroom.utils.Utils.str;
import static org.catness.classroom.utils.Utils.strDateTime;

/**
 * Created by catness on 5/10/19.
 */

public abstract class ReportsActivityCommon extends AppCompatActivity {
    private static final String LOG_TAG = ReportsActivityCommon.class.getSimpleName();
    protected ReportViewModel reportViewModel;
    private RewardViewModel rewardViewModel;
    protected TextView pointsDayView;
    protected LevelShow levelShow;

    protected int year, month, day;
    protected String dateText;
    protected RecyclerView recyclerView;
    protected boolean isSound = true;
    public boolean isDay;
    protected int maxpoints = MAXPOINTS;

    public void init() {
        pointsDayView = findViewById(R.id.pointsDayView);

        reportViewModel = ViewModelProviders.of(this).get(ReportViewModel.class);
        recyclerView = findViewById(R.id.recyclerview_reports);

        LinearLayoutManager l = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(l);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                l.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        levelShow = new LevelShow(this);

        rewardViewModel = ViewModelProviders.of(this).get(RewardViewModel.class);
        rewardViewModel.getAllRewards().observe(this, new Observer<List<Reward>>() {
            @Override
            public void onChanged(@Nullable final List<Reward> rewards) {
                // Update the cached copy of the words in the adapter.
                //adapter.setRewards(rewards);
            }
        });
    }

    public void launchUpdateReportActivity(Report report) {
        Intent intent = new Intent(this, NewReportActivity.class);
        intent.putExtra(SUBJECT, report.subject);
        intent.putExtra(COLOR, report.color);
        intent.putExtra(POINTS, report.points);
        intent.putExtra(COMMENT, report.comment);
        intent.putExtra(DATETIME, strDateTime(report.datetime));
        intent.putExtra(ID, report.id);
        Log.d(LOG_TAG, "Edit report: " + report.toString());
        startActivityForResult(intent, UPDATE_REPORT_ACTIVITY_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_THEME_REQUEST_CODE) {
            levelShow.updateThemeInfo();
            return;
        }

        if (resultCode == RESULT_CANCELED) {
            Log.d(LOG_TAG, "Result canceled");
            return;
        }
        switch (requestCode) {
            case NEW_REPORT_ACTIVITY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    MediaPlayer mp;
                    String subject = data.getStringExtra(SUBJECT);
                    String color = data.getStringExtra(COLOR);
                    int points = data.getIntExtra(POINTS, 0);
                    String comment = data.getStringExtra(COMMENT);
                    Long timestamp = data.getLongExtra(DATETIME, 0);
                    Report report = new Report(subject, color, timestamp, points, comment);
                    reportViewModel.insert(report, maxpoints);
                    if (levelShow.isLevelUp(points)) {
                        if (isSound) {
                            mp = MediaPlayer.create(this, R.raw.levelup);
                            mp.start();
                        }
                        levelShow.levelUpDialog(rewardViewModel);
                    } else {
                        if (isSound) {
                            if (points > 0)
                                mp = MediaPlayer.create(this, R.raw.positive);
                            else
                                mp = MediaPlayer.create(this, R.raw.negative);
                            mp.start();
                        }
                    }
                } else {
                    Toast.makeText(this, R.string.empty_not_saved, Toast.LENGTH_LONG).show();
                }
                break;
            case UPDATE_REPORT_ACTIVITY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    int id = data.getIntExtra(ID, 0);
                    if (data.getExtras().containsKey(DELETE)) {
                        reportViewModel.delete(id, maxpoints);
                    } else {
                        String subject = data.getStringExtra(SUBJECT);
                        String color = data.getStringExtra(COLOR);
                        int points = data.getIntExtra(POINTS, 0);
                        int lastpoints = data.getIntExtra(LASTPOINTS, 0);
                        String comment = data.getStringExtra(COMMENT);
                        Long timestamp = data.getLongExtra(DATETIME, 0);
                        Report report = new Report(id, subject, color, timestamp, points, comment);
                        reportViewModel.update(report, lastpoints, maxpoints);
                        if (levelShow.isLevelUp(points - lastpoints)) {
                            MediaPlayer mp = MediaPlayer.create(this, R.raw.levelup);
                            mp.start();
                            levelShow.levelUpDialog(rewardViewModel);
                        }
                    }
                } else {
                    Toast.makeText(this, R.string.empty_not_saved, Toast.LENGTH_LONG).show();
                }
                break;
        }

        if (resultCode == RESULT_OK) {
            final GetDayPoints getDayPoints = new GetDayPoints(this);
            getDayPoints.execute(year, month, day);
        }
    }

    protected static class GetDayPoints extends AsyncTask<Integer, Void, Integer> {
        String reportDate;
        private final WeakReference<ReportsActivityCommon> activityReference;

        // only retain a weak reference to the activity
        GetDayPoints(ReportsActivityCommon context) {
            activityReference = new WeakReference<>(context);
        }

        @Override
        protected Integer doInBackground(Integer... params) {
            int year = params[0];
            int month = params[1];
            int day = params[2];
            reportDate = String.format(Locale.ENGLISH, "%04d-%02d-%02d", year, month, day);
            Long start = startDay(year, month, day);
            Long end = endDay(year, month, day);
            Log.d(LOG_TAG, "getDayPoints bg task: year=" + str(year) + " month=" + str(month) + " day=" + str(day) + " start=" + str(start) + " end=" + str(end));
            // get a reference to the activity if it is still there
            ReportsActivityCommon activity = activityReference.get();
            if (activity == null || activity.isFinishing()) return 0;
            return activity.reportViewModel.getPointsDay(start, end);
        }

        @Override
        protected void onPostExecute(Integer points) {
            String dateToday = Utils.strDateToday();
            Log.d(LOG_TAG, "dateToday = " + dateToday + " reportDate=" + reportDate + " points=" + str(points));
            ReportsActivityCommon activity = activityReference.get();
            if (activity == null || activity.isFinishing()) return;
            if (reportDate.equals(dateToday)) {
                activity.levelShow.updateDayPoints(points);
            }
            if (activity.isDay) {
                if (reportDate.equals(activity.dateText)) {
                    String header = activity.getString(R.string.day_reports_header, activity.dateText, points);
                    activity.pointsDayView.setText(header);
                }
            } else {
                if (!activity.pointsDayView.getText().toString().equals(dateToday)) {
                    activity.pointsDayView.setText(dateToday);
                }

            }
        }
    }

}
