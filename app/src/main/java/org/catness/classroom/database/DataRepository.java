package org.catness.classroom.database;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;

import org.catness.classroom.levels.Level;
import org.catness.classroom.levels.LevelDao;
import org.catness.classroom.reports.Report;
import org.catness.classroom.reports.ReportDao;
import org.catness.classroom.rewards.Reward;
import org.catness.classroom.rewards.RewardDao;
import org.catness.classroom.subjects.Subject;
import org.catness.classroom.subjects.SubjectDao;
import org.catness.classroom.themes.Theme;
import org.catness.classroom.themes.ThemeDao;
import org.catness.classroom.themes.ThemeLevelDao;

import java.util.List;

import static org.catness.classroom.utils.Constants.POS_TMP;
import static org.catness.classroom.utils.Utils.str;

public class DataRepository {
    private static final String LOG_TAG = DataRepository.class.getSimpleName();
    private final SubjectDao subjectDao;
    private final ReportDao reportDao;
    private final LevelDao levelDao;
    private final RewardDao rewardDao;
    private final ThemeDao themeDao;

    private final LiveData<List<Subject>> subjects;
    //  private LiveData<List<Report>> reports;
    //  private LiveData<List<Report>> reportsDay;
    private final LiveData<Level> level;
    private final LiveData<Integer> maxpos;
    private final LiveData<List<Reward>> rewards;
    private final LiveData<List<Theme>> themes;


    public DataRepository(Application application) {
        SchoolDatabase db = SchoolDatabase.getDatabase(application);
        subjectDao = db.subjectDao();
        subjects = subjectDao.getAllSubjects();
        maxpos = subjectDao.getMaxPos();
        reportDao = db.reportDao();
        rewardDao = db.rewardDao();
        rewards = rewardDao.getAllRewards();
        levelDao = db.levelDao();
        level = levelDao.getLevel();
//        reports = reportDao.getAllReports();
        themeDao = db.themeDao();
        themes = themeDao.getAllThemes();
    }


    public List<Report> getAllReportsSync() {
        return reportDao.getAllReportsSync();
    }

    public List<Subject> getAllSubjectsSync() {
        return subjectDao.getAllSubjectsSync();
    }

    public List<Reward> getAllRewardsSync() {
        return rewardDao.getAllRewardsSync();
    }

    public Level getLevelSync() {
        return levelDao.getLevelSimple();
    }

    public LiveData<List<Subject>> getAllSubjects() {
        return subjects;
    }

    public LiveData<List<Report>> getAllReports(Integer limit) {
        return reportDao.getAllReports(limit);
    }

    public LiveData<Level> getLevel() {
        return level;
    }

    public LiveData<Integer> getMaxPos() {
        return maxpos;
    }

    public LiveData<List<Reward>> getAllRewards() {
        return rewards;
    }

    public LiveData<List<Report>> getAllReportsDay(Long start, Long end) {
        return reportDao.getAllReportsDay(start, end);
    }

    public void insertReward(Reward reward) {
        new insertRewardAsyncTask(rewardDao).execute(reward);
    }

    public void deleteReward(int id) {
        new deleteRewardAsyncTask(rewardDao).execute(id);
    }

    public void updateReward(Reward reward) {
        new updateRewardAsyncTask(rewardDao).execute(reward);
    }

    public void decrementReward(int id) {
        new decrementRewardAsyncTask(rewardDao).execute(id);
    }


    public void insertSubject(Subject subject) {
        new insertSubjectAsyncTask(subjectDao).execute(subject);
    }

    public void deleteSubject(int id) {
        new deleteSubjectAsyncTask(subjectDao).execute(id);
    }

    public void updateSubject(Subject subject) {
        new updateSubjectAsyncTask(subjectDao).execute(subject);
    }

    public void updateSubjectPos(int id, int pos) {
        new updateSubjectPosAsyncTask(subjectDao).execute(id, pos);
    }

    public void recalculateSubjectPositions() {
        new recalculateSubjectPositionsAsyncTask(subjectDao).execute();
    }

    public void swapSubjectPosSync(int from, int to) {
        Log.d(LOG_TAG, "swap subjects: " + str(from) + " -> " + str(to));
        Log.d(LOG_TAG, "update subjects set pos=" + str(POS_TMP) + " where pos=" + str(from));
        subjectDao.updatePosStart(from, POS_TMP);
        if (from < to) {
            Log.d(LOG_TAG, "update subjects set pos=pos-1 where pos>" + str(from) + " and pos<=" + str(to));
            subjectDao.updatePosMoveUp(from, to);
        } else {
            Log.d(LOG_TAG, "update subjects set pos=pos+1 where pos>=" + str(to) + " and pos<" + str(from));
            subjectDao.updatePosMoveDown(from, to);
        }
        Log.d(LOG_TAG, "update subjects set pos=" + str(to) + " where pos=" + str(POS_TMP));
        subjectDao.updatePosEnd(to, POS_TMP);

        Log.d(LOG_TAG, "swap complete in data repository");
        List<Subject> sub = subjectDao.getAllSubjectsSync();
        for (Subject s : sub) {
            Log.d(LOG_TAG, s.toString());
        }
    }

    public void deleteAllSubjects() {
        new deleteAllSubjectsAsyncTask(subjectDao).execute();
    }

    public void deleteAllRewards() {
        new deleteAllRewardsAsyncTask(rewardDao).execute();
    }


    public void insertReport(Report report, int maxpoints) {
        new insertReportAsyncTask(reportDao, levelDao, maxpoints).execute(report);
    }

    public void deleteReport(int id, int maxpoints) {
        new deleteReportAsyncTask(reportDao, levelDao, maxpoints).execute(id);
    }

    public void updateReport(Report report, int points, int maxpoints) {
        new updateReportAsyncTask(reportDao, levelDao, points, maxpoints).execute(report);
    }

    public void deleteAllReports() {
        new deleteAllReportsAsyncTask(reportDao).execute();
    }

    public void updateLevel(Level level) {
        new updateLevelAsyncTask(levelDao).execute(level);
    }

    private static class updateLevelAsyncTask extends AsyncTask<Level, Void, Void> {
        private final LevelDao mAsyncTaskDao;

        updateLevelAsyncTask(LevelDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Level... params) {
            Log.d(LOG_TAG, " update level : " + params[0].toString());
            mAsyncTaskDao.update(params[0]);
            return null;
        }
    }

    private static class insertRewardAsyncTask extends AsyncTask<Reward, Void, Void> {
        private final RewardDao mAsyncTaskDao;

        insertRewardAsyncTask(RewardDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Reward... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }


    private static class insertSubjectAsyncTask extends AsyncTask<Subject, Void, Void> {
        private final SubjectDao mAsyncTaskDao;

        insertSubjectAsyncTask(SubjectDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Subject... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class deleteRewardAsyncTask extends AsyncTask<Integer, Void, Void> {
        private final RewardDao mAsyncTaskDao;

        deleteRewardAsyncTask(RewardDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            mAsyncTaskDao.delete(params[0]);
            return null;
        }
    }


    private static class decrementRewardAsyncTask extends AsyncTask<Integer, Void, Void> {
        private final RewardDao mAsyncTaskDao;

        decrementRewardAsyncTask(RewardDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            mAsyncTaskDao.decrement(params[0]);
            return null;
        }
    }


    private static class deleteAllSubjectsAsyncTask extends AsyncTask<Void, Void, Void> {
        private final SubjectDao mAsyncTaskDao;

        deleteAllSubjectsAsyncTask(SubjectDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Log.d(LOG_TAG, "Deleting all subjects...");
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }

    private static class deleteAllRewardsAsyncTask extends AsyncTask<Void, Void, Void> {
        private final RewardDao mAsyncTaskDao;

        deleteAllRewardsAsyncTask(RewardDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }

    private static class deleteSubjectAsyncTask extends AsyncTask<Integer, Void, Void> {
        private final SubjectDao mAsyncTaskDao;

        deleteSubjectAsyncTask(SubjectDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            mAsyncTaskDao.delete(params[0]);
            return null;
        }
    }

    private static class updateSubjectPosAsyncTask extends AsyncTask<Integer, Void, Void> {
        private final SubjectDao mAsyncTaskDao;

        updateSubjectPosAsyncTask(SubjectDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            Log.v(LOG_TAG, "update subject id=" + str(params[0]) + " to pos=" + str(params[1]));
            mAsyncTaskDao.updatePos(params[0], params[1]);
            return null;
        }
    }


    private static class recalculateSubjectPositionsAsyncTask extends AsyncTask<Void, Void, Void> {
        private final SubjectDao mAsyncTaskDao;

        recalculateSubjectPositionsAsyncTask(SubjectDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            List<Subject> subjects = mAsyncTaskDao.getAllSubjectsSync();
            int pos = 0;
            for (Subject subject : subjects) {
                int id = subject.id;
                Log.d(LOG_TAG, "~~~update " + str(id) + " to pos=" + str(pos));
                mAsyncTaskDao.updatePos(id, pos);
                pos++;
            }
            return null;
        }
    }


    private static class updateSubjectAsyncTask extends AsyncTask<Subject, Void, Void> {
        private final SubjectDao mAsyncTaskDao;

        updateSubjectAsyncTask(SubjectDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Subject... params) {
            Log.d(LOG_TAG, " update subject : " + str(params[0].id) + " " + params[0].title);
            mAsyncTaskDao.update(params[0]);
            return null;
        }
    }

    private static class updateRewardAsyncTask extends AsyncTask<Reward, Void, Void> {
        private final RewardDao mAsyncTaskDao;

        updateRewardAsyncTask(RewardDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Reward... params) {
            Log.d(LOG_TAG, " update reward : " + str(params[0].id) + " " + params[0].name);
            mAsyncTaskDao.update(params[0]);
            return null;
        }
    }


    private static class insertReportAsyncTask extends AsyncTask<Report, Void, Void> {
        private final ReportDao mAsyncTaskDao;
        private final LevelDao levelDao;
        private final int maxpoints;

        insertReportAsyncTask(ReportDao dao, LevelDao levelDao, int maxpoints) {
            mAsyncTaskDao = dao;
            this.levelDao = levelDao;
            this.maxpoints = maxpoints;
        }

        @Override
        protected Void doInBackground(final Report... params) {
            mAsyncTaskDao.insert(params[0]);

            int points = params[0].points;
            Level level = levelDao.getLevelSimple();
            level.update(points, maxpoints);
            Log.d(LOG_TAG, "================== update level: " + level.toString());
            levelDao.update(level);
            return null;
        }
    }


    private static class deleteAllReportsAsyncTask extends AsyncTask<Void, Void, Void> {
        private final ReportDao mAsyncTaskDao;

        deleteAllReportsAsyncTask(ReportDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }

    private static class deleteReportAsyncTask extends AsyncTask<Integer, Void, Void> {
        private final ReportDao mAsyncTaskDao;
        private final LevelDao levelDao;
        private final int maxpoints;

        deleteReportAsyncTask(ReportDao dao, LevelDao levelDao, int maxpoints) {
            mAsyncTaskDao = dao;
            this.levelDao = levelDao;
            this.maxpoints = maxpoints;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            int id = params[0];
            Report report = mAsyncTaskDao.getReportSimple(id);
            Log.d(LOG_TAG, "deleting report: " + report.toString());
            int points = report.points;
            mAsyncTaskDao.delete(id);
            Level level = levelDao.getLevelSimple();
            level.update(-points, maxpoints);
            Log.d(LOG_TAG, "================== update level: " + level.toString());
            levelDao.update(level);
            return null;
        }
    }

    private static class updateReportAsyncTask extends AsyncTask<Report, Void, Void> {
        private final ReportDao mAsyncTaskDao;
        private final LevelDao levelDao;
        private final int points, maxpoints;

        updateReportAsyncTask(ReportDao dao, LevelDao levelDao, int points, int maxpoints) {
            mAsyncTaskDao = dao;
            this.levelDao = levelDao;
            this.points = points;
            this.maxpoints = maxpoints;
        }

        @Override
        protected Void doInBackground(final Report... params) {
            Log.d(LOG_TAG, " update report : " + str(params[0].id) + " " + params[0].subject + " " + str(params[0].points));
            mAsyncTaskDao.update(params[0]);
            Level level = levelDao.getLevelSimple();
            level.update(-points + params[0].points, maxpoints); // remove the previous points and add the new ones
            Log.d(LOG_TAG, "================== update level: " + level.toString());
            levelDao.update(level);
            return null;
        }
    }

    public int getPointsDay(Long start, Long end) {
        return reportDao.getPointsDay(start, end);
    }


    // ***** Sync functions for import (they are called from async task anyway, so it doesn't make sense to create another async task for it)

    public void deleteAllSubjectsSync() {
        subjectDao.deleteAll();
    }

    public void deleteAllReportsSync() {
        reportDao.deleteAll();
    }

    public void deleteAllRewardsSync() {
        rewardDao.deleteAll();
    }

    public void insertAllSubjectsSync(List<Subject> subjects) {
        for (Subject subject : subjects) {
            subjectDao.insert(subject);
        }
    }

    public void insertAllReportsSync(List<Report> reports) {
        for (Report report : reports) {
            reportDao.insert(report);
        }
    }

    public void insertAllRewardsSync(List<Reward> rewards) {
        for (Reward reward : rewards) {
            rewardDao.insert(reward);
        }
    }

    public void updateLevelSync(Level level) {
        levelDao.update(level);
    }

    public LiveData<List<Theme>> getAllThemes() {
        return themes;
    }

    public void insertTheme(Theme theme) {
        new insertThemeAsyncTask(themeDao).execute(theme);
    }

    public void updateTheme(Theme theme) {
        new updateThemeAsyncTask(themeDao).execute(theme);
    }

    private static class updateThemeAsyncTask extends AsyncTask<Theme, Void, Void> {
        private final ThemeDao mAsyncTaskDao;

        updateThemeAsyncTask(ThemeDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Theme... params) {
            Theme t = params[0];
            Log.d(LOG_TAG, " update theme : " + t.toString());
            mAsyncTaskDao.update(params[0]);
            return null;
        }
    }


    private static class insertThemeAsyncTask extends AsyncTask<Theme, Void, Void> {
        private final ThemeDao mAsyncTaskDao;

        insertThemeAsyncTask(ThemeDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Theme... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }


}
