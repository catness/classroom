package org.catness.classroom.database;


import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.catness.classroom.MainActivity;

import java.io.File;
import java.io.FileInputStream;
import java.lang.ref.WeakReference;

import static org.catness.classroom.utils.Constants.DATABASE_NAME;
import static org.catness.classroom.utils.Constants.IMPORT_FILENAME;
import static org.catness.classroom.utils.Constants.IMPORT_FILENAME_SAVE;
import static org.catness.classroom.utils.Utils.str;

public class ImportDatabase extends AsyncTask<Void, Void, String> {
    private static final String LOG_TAG = ImportDatabase.class.getSimpleName();
    private final WeakReference<MainActivity> activityReference;

    public ImportDatabase(MainActivity context) {
        this.activityReference = new WeakReference<>(context);
    }

    @Override
    protected String doInBackground(Void... voids) {
        // get a reference to the activity if it is still there
        MainActivity activity = activityReference.get();
        if (activity == null || activity.isFinishing()) return "";

        File fileDir = new File(activity.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), DATABASE_NAME);
        File backupFile = new File(fileDir, IMPORT_FILENAME);
        FileInputStream input;
        try {
            Log.d(LOG_TAG, "trying to import from :" + backupFile.getAbsolutePath());
            input = new FileInputStream(backupFile);
        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage());
            return "File " + backupFile.getAbsolutePath() + " not found.";
        }
        ObjectMapper objectMapper = new ObjectMapper();
        DataAllObjects data;
        try {
            data = objectMapper.readValue(input, DataAllObjects.class);
        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage());
            return " File " + backupFile.getAbsolutePath() + " is in invalid format - can't be imported.";
        }

        data.importAll(activity.getApplication());

        File backupFileSave = new File(fileDir, IMPORT_FILENAME_SAVE);
        boolean success = backupFile.renameTo(backupFileSave);
        Log.d(LOG_TAG, "rename returns " + str(success));
        return "Import completed!";
    }

    @Override
    protected void onPostExecute(String message) {
        MainActivity activity = activityReference.get();
        if (activity == null || activity.isFinishing()) return;
        activity.pointsToday();
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }
}