package org.catness.classroom.database;

import android.app.Application;

import org.catness.classroom.levels.Level;
import org.catness.classroom.reports.Report;
import org.catness.classroom.rewards.Reward;
import org.catness.classroom.subjects.Subject;

import java.util.List;

// This is for export/import (json)
public class DataAllObjects {
    public List<Subject> subjects;
    public List<Report> reports;
    public Level level;
    public List<Reward> rewards;
    private DataRepository repository;

    // for .json import (and the fields are public for the same reason)
    public DataAllObjects() {
    }

    public DataAllObjects(Application application) {
        repository = new DataRepository(application);
        reports = repository.getAllReportsSync();
        subjects = repository.getAllSubjectsSync();
        rewards = repository.getAllRewardsSync();
        level = repository.getLevelSync();
    }

    public void importAll(Application application) {
        DataRepository repository = new DataRepository(application);
        repository.deleteAllSubjectsSync();
        repository.insertAllSubjectsSync(subjects);
        repository.deleteAllReportsSync();
        repository.insertAllReportsSync(reports);
        repository.deleteAllRewardsSync();
        repository.insertAllRewardsSync(rewards);
        repository.updateLevelSync(level);
    }


}
