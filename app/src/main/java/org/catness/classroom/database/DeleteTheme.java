package org.catness.classroom.database;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import org.catness.classroom.ThemesSelectActivity;
import org.catness.classroom.themes.ThemeDao;
import org.catness.classroom.themes.ThemeLevel;
import org.catness.classroom.themes.ThemeLevelDao;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.List;

import static org.catness.classroom.utils.Constants.DATABASE_NAME;

/**
 * Created by catness on 6/15/19.
 */

public class DeleteTheme extends AsyncTask<Void, Void, String> {
    private static final String LOG_TAG = DeleteTheme.class.getSimpleName();
    private final WeakReference<ThemesSelectActivity> activityReference;
    private final int themeID;

    public DeleteTheme(ThemesSelectActivity context, int themeID) {
        this.activityReference = new WeakReference<>(context);
        this.themeID = themeID;
    }

    @Override
    protected String doInBackground(Void... voids) {
        Log.d(LOG_TAG, "Deleting theme : " + themeID);
        ThemesSelectActivity activity = activityReference.get();
        if (activity == null || activity.isFinishing()) return "";

        SchoolDatabase db = SchoolDatabase.getDatabase(activity.getApplication());
        ThemeDao themeDao = db.themeDao();
        ThemeLevelDao themeLevelDao = db.themeLevelDao();

        File fileDir = new File(activity.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), DATABASE_NAME);
        String path = themeDao.getPath(themeID);
        List<ThemeLevel> levels = themeLevelDao.getAllThemeLevelsSync(themeID);
        for (ThemeLevel level : levels) {
            String filename = level.image;
            File image = new File(fileDir, path + "/" + filename);
            Log.d(LOG_TAG, "deleting file: " + image);
            image.delete();
        }
        File imageDir = new File(fileDir, path);
        imageDir.delete();
        themeLevelDao.deleteAll(themeID);
        themeDao.delete(themeID);
        return null;
    }
}
