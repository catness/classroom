package org.catness.classroom.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import org.catness.classroom.levels.Level;
import org.catness.classroom.levels.LevelDao;
import org.catness.classroom.reports.Report;
import org.catness.classroom.reports.ReportDao;
import org.catness.classroom.rewards.Reward;
import org.catness.classroom.rewards.RewardDao;
import org.catness.classroom.subjects.Subject;
import org.catness.classroom.subjects.SubjectDao;
import org.catness.classroom.themes.Theme;
import org.catness.classroom.themes.ThemeDao;
import org.catness.classroom.themes.ThemeLevel;
import org.catness.classroom.themes.ThemeLevelDao;

import static org.catness.classroom.database.ExportDatabase.createImportDir;
import static org.catness.classroom.utils.Constants.DATABASE_NAME;
import static org.catness.classroom.utils.Constants.THEME_DEFAULT;
import static org.catness.classroom.utils.Utils.dateToTimestamp;

@Database(entities = {Subject.class, Report.class, Level.class, Reward.class, Theme.class, ThemeLevel.class}, version = 4, exportSchema = false)
public abstract class SchoolDatabase extends RoomDatabase {
    private static final String LOG_TAG = SchoolDatabase.class.getSimpleName();

    public abstract SubjectDao subjectDao();

    public abstract ReportDao reportDao();

    public abstract LevelDao levelDao();

    public abstract RewardDao rewardDao();

    public abstract ThemeDao themeDao();

    public abstract ThemeLevelDao themeLevelDao();

    private static SchoolDatabase INSTANCE;

    public static SchoolDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (SchoolDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            SchoolDatabase.class, DATABASE_NAME)
                            .addMigrations(MIGRATION_1_2, MIGRATION_2_3, MIGRATION_3_4)
                            // .fallbackToDestructiveMigration()
                            .addCallback(initialize)
                            .build();
                    createImportDir(context);
                }
            }
        }
        return INSTANCE;
    }

    private static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("alter table subjects add column pos integer default 0 not null");
        }
    };


    private static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE `rewards` (`id` INTEGER not null, `name` TEXT not null, `description` TEXT, `weight` INTEGER not null, `amount` INTEGER not null, PRIMARY KEY(`id`))");
        }
    };

    private static final Migration MIGRATION_3_4 = new Migration(3, 4) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE `themes` (`id` INTEGER not null, `name` TEXT not null, `path` TEXT not null, `count` INTEGER not null, `comment` TEXT, PRIMARY KEY(`id`))");
            database.execSQL("CREATE TABLE `themelevels` (`id` INTEGER not null, `name` TEXT not null, `theme` INTEGER not null, `level` INTEGER not null, `image` TEXT, PRIMARY KEY(`id`))");
        }
    };

    private static final RoomDatabase.Callback initialize =
            new RoomDatabase.Callback() {

                @Override
                public void onOpen(@NonNull SupportSQLiteDatabase db) {
                    Log.d(LOG_TAG, "..............open database!");
                    super.onOpen(db);
                    new initializeAsync(INSTANCE).execute();
                }
            };

    /**
     * Populate the database in the background.
     */
    private static class initializeAsync extends AsyncTask<Void, Void, Void> {

        private final SubjectDao subjectDao;
        private final ReportDao reportDao;
        private final LevelDao levelDao;
        private final RewardDao rewardDao;
        private final ThemeDao themeDao;

        initializeAsync(SchoolDatabase db) {
            subjectDao = db.subjectDao();
            reportDao = db.reportDao();
            levelDao = db.levelDao();
            rewardDao = db.rewardDao();
            themeDao = db.themeDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            // Start the app with a clean database every time.
            // Not needed if you only populate the database
            // when it is first created
            ///// subjectDao.deleteAll();
            Log.d("Database", "................Initialize ");
            if (subjectDao.getCount() == 0) {
                subjectDao.insert(new Subject("Learning", "Online courses, textbooks etc", "#904040", 0));
                subjectDao.insert(new Subject("Reading", "Reading books", "#409090", 1));
                subjectDao.insert(new Subject("Trying something new", "Trying something new", "#5deff0", 2));
            }
            if (reportDao.getCount() == 0) {
                reportDao.insert(new Report("Trying something new", "#5deff0", dateToTimestamp(""), 10, "Installed this app"));
            }
            if (levelDao.getCount() == 0) {
                levelDao.insert(new Level(0, 0, 10));
            }
            if (rewardDao.getCount() == 0) {
                rewardDao.insert(new Reward("Tasty snack", "A tasty snack...", 1, 4));
            }
            if (themeDao.getCount() == 0) {
                themeDao.insert(new Theme(THEME_DEFAULT, "Tarot", ".", 78, ""));
            }
            Log.d("Database", "................init complete!");
            return null;
        }
    }

}
