package org.catness.classroom.database;

import android.app.Activity;
import android.app.Application;
import android.os.Environment;
import android.util.Log;

import org.catness.classroom.themes.Theme;
import org.catness.classroom.themes.ThemeDao;
import org.catness.classroom.themes.ThemeLevel;
import org.catness.classroom.themes.ThemeLevelDao;
import org.catness.classroom.themes.ThemeLevelData;

import java.io.File;
import java.util.List;

import static org.catness.classroom.utils.Constants.DATABASE_NAME;
import static org.catness.classroom.utils.Utils.str;

/**
 * Created by catness on 6/14/19.
 */

public class DataTheme {
    private static final String LOG_TAG = DataTheme.class.getSimpleName();
    public String name;
    public List<ThemeLevelData> levels;

    public DataTheme() {
    }

    public void importAll(Activity activity, String path) {
        Log.d(LOG_TAG, "name = " + name);
        int count = levels.size();
        Application application = activity.getApplication();
        SchoolDatabase db = SchoolDatabase.getDatabase(application);
        ThemeDao themeDao = db.themeDao();
        ThemeLevelDao themeLevelDao = db.themeLevelDao();

        int checkID = themeDao.getId(name);
        Log.d(LOG_TAG, "checking theme with name=" + name + ": id=" + str(checkID));
        if (checkID != 0) {
            Log.d(LOG_TAG, "theme with name=" + name + " already exists.");
            return;
        }

        Theme theme = new Theme(name, path, count, "");
        themeDao.insert(theme);
        int themeID = themeDao.getId(name);
        Log.d(LOG_TAG, "inserted theme id=" + themeID);

        File fileDir = new File(activity.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), DATABASE_NAME);
        String themePath = themeDao.getPath(themeID);

        int level = 0;
        boolean missing = false;
        StringBuilder comments = new StringBuilder("Missing images: ");
        for (ThemeLevelData data : levels) {
            ThemeLevel themeLevel = new ThemeLevel(data.name, themeID, level, data.image);
            themeLevelDao.insert(themeLevel);
            File image = new File(fileDir, themePath + "/" + data.image);
            if (!image.exists()) {
                Log.d(LOG_TAG, "missing file : " + image.getAbsolutePath());
                comments.append(data.image).append(" ");
                missing = true;
            } else {
                Log.d(LOG_TAG, "file : " + image.getAbsolutePath());
            }
            Log.d(LOG_TAG, "added level : " + themeLevel.toString());
            level++;
        }
        if (missing) {
            String comment = comments.toString();
            Log.d(LOG_TAG, "missing files : " + comment);
            themeDao.update(new Theme(themeID, name, path, count, comment));
        }
    }

}
