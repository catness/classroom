package org.catness.classroom.database;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.catness.classroom.MainActivity;
import org.catness.classroom.R;

import java.io.File;
import java.io.FileWriter;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static org.catness.classroom.utils.Constants.DATABASE_NAME;
import static org.catness.classroom.utils.Constants.EXPORT_EMAIL;
import static org.catness.classroom.utils.Constants.EXPORT_EXTERNAL;
import static org.catness.classroom.utils.Constants.EXPORT_INTERNAL;
import static org.catness.classroom.utils.Constants.EXPORT_SDCARD;
import static org.catness.classroom.utils.Utils.str;
import static org.catness.classroom.utils.Utils.strDateTime;

// how to pretty-print the file contents on command line (after sending the file to the user) :
// python3 -m json.tool < db_backup_whateveristhename.json

public class ExportDatabase extends AsyncTask<Void, Void, File> {
    private static final String LOG_TAG = ExportDatabase.class.getSimpleName();
    private final int exportMode;
    private final String appHeader;
    private final WeakReference<MainActivity> activityReference;

    public ExportDatabase(MainActivity context, int exportMode, String appHeader) {
        this.activityReference = new WeakReference<>(context);
        this.exportMode = exportMode;
        this.appHeader = appHeader;
    }

    /* Checks if external storage is available for read and write */
    private static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public static void createImportDir(Context context) {
        // upon installing the app, try to create an empty directory to put import files
        // /storage/emulated/0/Android/data/org.catness.classroom/files/Documents/schooldata/
        // can access it with Ghost Commander
        File dir = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), DATABASE_NAME);
        if (!dir.mkdirs()) {
            Log.d(LOG_TAG, "error creating import directory");
        }
    }

    private static File getPrivateStorageDir(Context context, int mode) {
        File file = null;

        switch (mode) {
            case EXPORT_SDCARD:
                File[] files = context.getExternalFilesDirs(Environment.DIRECTORY_DOCUMENTS);
                for (File f : files) {
                    String path = f.getAbsolutePath();
                    Log.d(LOG_TAG, "external storage : " + path);
                    if (!path.contains("/emulated/")) {
                        file = new File(f, DATABASE_NAME);
                        break;
                    }
                }
                if (file != null) break;
                // if SDCard not available (all directories are "emulated"), fall through
                // to export_external
            case EXPORT_EXTERNAL:
                File fdefault = context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
                // /storage/emulated/0/Android/data/org.catness.classroom/files/Documents/schooldata/
                // can access it with Ghost Commander
                Log.d(LOG_TAG, "default path : " + fdefault);
                file = new File(fdefault, DATABASE_NAME);
                break;
            case EXPORT_INTERNAL:
                // /data/data/org.catness.classroom/files (using Device Explorer in Android Studio)
                // currently unused, because other apps can't access this directory
                file = context.getFilesDir();
                break;
            case EXPORT_EMAIL:
                // /data/data/org.catness.classroom/cache/ (using Device Explorer in Android Studio)
                file = context.getCacheDir();
                break;
        }
        try {
            if (!file.mkdirs()) {
                Log.d(LOG_TAG, "Directory not created");
            }
        } catch (Exception e) {
            Log.d(LOG_TAG, "exception while creating directories: " + e.getMessage());
        }
        return file;
    }

    private static String createBackupFileName() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HHmm", Locale.ENGLISH);
        return "db_backup_" + sdf.format(new Date()) + ".json";
    }

    private static File exportDb(Context context, String content, int mode) {
        boolean ret = isExternalStorageWritable();
        if (!ret) {
            //Toast.makeText(context,"External storage not available",Toast.LENGTH_LONG).show();
            Log.d(LOG_TAG, "external storage not available");
            return null;
        }

        File backupDir = getPrivateStorageDir(context, mode);
        String fileName = createBackupFileName();
        Log.d(LOG_TAG, "Creating backup file: " + backupDir + " ------- " + fileName);
        File backupFile = new File(backupDir, fileName);
        try {
            boolean success = backupFile.createNewFile();
            Log.d(LOG_TAG, "result: " + str(success) + " " + backupFile.getAbsolutePath());
            //Toast.makeText(context,"File created: " + backupFile.getAbsolutePath(),Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage());
            return null;
        }
        try {
            FileWriter fileWriter = new FileWriter(backupFile);
            fileWriter.write(content);
            fileWriter.close();
            return backupFile;
        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage());
        }
        return null;
    }

    @Override
    protected File doInBackground(Void... voids) {
        // get a reference to the activity if it is still there
        MainActivity activity = activityReference.get();
        if (activity == null || activity.isFinishing()) return null;
        Application app = activity.getApplication();

        ObjectMapper obj = new ObjectMapper();
        DataAllObjects data = new DataAllObjects(app);
        File file = null;
        try {
            String jsonStr = obj.writeValueAsString(data);
            file = exportDb(activity, jsonStr, exportMode);
        } catch (Exception e) {
            Log.d(LOG_TAG, "exception " + e.getMessage());
        }
        return file;
    }

    @Override
    protected void onPostExecute(File file) {
       // get a reference to the activity if it is still there
        MainActivity activity = activityReference.get();
        if (activity == null || activity.isFinishing()) return;
        if (file == null) {
            Toast.makeText(activity, "Error saving backup file", Toast.LENGTH_LONG).show();
            return;
        }
        if (exportMode == EXPORT_EMAIL) {
            emailData(file,activity);
            file.deleteOnExit();
        } else {
            Toast.makeText(activity, "File created: " + file.getAbsolutePath(), Toast.LENGTH_LONG).show();
        }
    }

    private void emailData(File file,Context context) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        // The intent does not have a URI, so declare the "text/plain" MIME type
        emailIntent.setType("text/plain");
        //emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"jon@example.com"}); // recipients
        String backupSubject = context.getString(R.string.backup_email_subject, appHeader, strDateTime());
        String backupMessage = context.getString(R.string.backup_email_message, appHeader, strDateTime());
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, backupSubject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, backupMessage);
        //Uri uri = Uri.fromFile(file);
        Uri uri = FileProvider.getUriForFile(
                context,
                "org.catness.classroom.provider", //(use your app signature + ".provider" )
                file);
        emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
        context.startActivity(emailIntent);
    }
}
