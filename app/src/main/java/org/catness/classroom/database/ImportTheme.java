package org.catness.classroom.database;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.catness.classroom.ThemesSelectActivity;
import org.catness.classroom.themes.ThemeDao;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import static org.catness.classroom.utils.Constants.DATABASE_NAME;
import static org.catness.classroom.utils.Constants.THEME_FILENAME;
import static org.catness.classroom.utils.Utils.str;

/**
 * Created by catness on 6/14/19.
 */

/*
Upload the theme.zip file and put it into the app directory:

/storage/emulated/0/Android/data/org.catness.classroom/files/Documents/schooldata/

*/

public class ImportTheme extends AsyncTask<Void, Void, String> {
    private static final String LOG_TAG = ImportTheme.class.getSimpleName();
    private final WeakReference<ThemesSelectActivity> activityReference;

    public ImportTheme(ThemesSelectActivity context) {
        this.activityReference = new WeakReference<>(context);
    }

    @Override
    protected void onPostExecute(String message) {
        ThemesSelectActivity activity = activityReference.get();
        if (activity == null || activity.isFinishing()) return;
        if (message.equals("")) return;
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(Void... voids) {
        // get a reference to the activity if it is still there
        ThemesSelectActivity activity = activityReference.get();
        if (activity == null || activity.isFinishing()) return "";

        File fileDir = new File(activity.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), DATABASE_NAME);
        File themeFile = new File(fileDir, THEME_FILENAME);
        Log.d(LOG_TAG, "themeFile: " + themeFile.toString());
        try {
            ZipFile zipFile = new ZipFile(themeFile);
            ZipEntry entry = zipFile.getEntry("theme.json");
            InputStream is = zipFile.getInputStream(entry);
            byte[] jsonData = new byte[1024];
            StringBuilder jsonBuilder = new StringBuilder();
            while (is.read(jsonData) >= 0) {
                jsonBuilder.append(new String(jsonData));
            }
            String json = jsonBuilder.toString();
            Log.d(LOG_TAG, "theme.json: " + json);
            ObjectMapper objectMapper = new ObjectMapper();
            DataTheme data;
            try {
                data = objectMapper.readValue(json, DataTheme.class);
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage());
                return " Theme file is in invalid format - can't be imported.";
            }
            String themeName = data.name;
            SchoolDatabase db = SchoolDatabase.getDatabase(activity.getApplication());
            ThemeDao themeDao = db.themeDao();
            int themeID = themeDao.getId(themeName);
            if (themeID != 0) {
                return "Theme with the name " + themeName + " already exists.";
            }
            String targetDirBase = themeName.replaceAll("[^a-zA-Z0-9]", "_");
            String target = targetDirBase;
            File targetDir;
            int cnt = 1;
            do {
                targetDir = new File(fileDir, target);
                if (!targetDir.exists()) break;
                target = targetDirBase + str(cnt);
                cnt++;
            } while (cnt < 1000);
            if (cnt >= 1000) {
                return "Can't create a directory for " + targetDirBase;
            }
            if (!targetDir.mkdirs()) {
                return "Can't create directory : " + targetDir.getAbsolutePath();
            }
            unzip(themeFile, targetDir);
            data.importAll(activity, target);
            return "Theme " + data.name + " imported successfully!";
        } catch (Exception e) {
            Log.d(LOG_TAG, "can't read zip file " + e.getMessage());
            return "Error reading the theme zip file : " + e.getMessage();
        }
    }

    // https://stackoverflow.com/questions/3382996/how-to-unzip-files-programmatically-in-android
    private static void unzip(File zipFile, File targetDirectory) throws IOException {
        Log.d(LOG_TAG, "starting to unzip file " + zipFile.getAbsolutePath() + " into " + targetDirectory.getAbsolutePath());
        try (ZipInputStream zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile)))) {
            ZipEntry ze;
            int count;
            byte[] buffer = new byte[8192];
            while ((ze = zis.getNextEntry()) != null) {
                String name = ze.getName();
                if (name.endsWith(".json")) continue;
                File file = new File(targetDirectory, name);
                Log.d(LOG_TAG, "unzipping name=" + file.toString());
                File dir = ze.isDirectory() ? file : file.getParentFile();
                if (!dir.isDirectory() && !dir.mkdirs())
                    throw new FileNotFoundException("Failed to ensure directory: " + dir.getAbsolutePath());
                if (ze.isDirectory()) continue;
                try (FileOutputStream fout = new FileOutputStream(file)) {
                    while ((count = zis.read(buffer)) != -1)
                        fout.write(buffer, 0, count);
                }
            }
        }
    }

}
