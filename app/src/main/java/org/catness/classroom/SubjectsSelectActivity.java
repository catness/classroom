package org.catness.classroom;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.catness.classroom.subjects.Subject;
import org.catness.classroom.subjects.SubjectListAdapter;
import org.catness.classroom.subjects.SubjectViewModel;
import org.catness.classroom.utils.SimpleItemTouchHelperCallback;

import java.lang.ref.WeakReference;
import java.util.List;

import static org.catness.classroom.utils.Constants.COLOR;
import static org.catness.classroom.utils.Constants.DATETIME;
import static org.catness.classroom.utils.Constants.DELETE;
import static org.catness.classroom.utils.Constants.DESCRIPTION;
import static org.catness.classroom.utils.Constants.ID;
import static org.catness.classroom.utils.Constants.NEW_REPORT_ACTIVITY_REQUEST_CODE;
import static org.catness.classroom.utils.Constants.NEW_SUBJECT_ACTIVITY_REQUEST_CODE;
import static org.catness.classroom.utils.Constants.POS;
import static org.catness.classroom.utils.Constants.PREF_APP_HEADER;
import static org.catness.classroom.utils.Constants.REPORTS;
import static org.catness.classroom.utils.Constants.SUBJECT;
import static org.catness.classroom.utils.Constants.TITLE;
import static org.catness.classroom.utils.Constants.TOUCH_DRAG;
import static org.catness.classroom.utils.Constants.UPDATE_SUBJECT_ACTIVITY_REQUEST_CODE;
import static org.catness.classroom.utils.Utils.str;

public class SubjectsSelectActivity extends AppCompatActivity {
    private static final String LOG_TAG = SubjectsSelectActivity.class.getSimpleName();
    private SubjectViewModel subjectViewModel;
    private boolean isReports = false; // false if just editing subjects, true if selecting a subject for the new report
    private boolean isDate = false; // true if coming from calendar page, false if from main page
    private String dateTime;
    private SubjectListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (((App) getApplicationContext()).isNightModeEnabled())
            setTheme(R.style.ActivityThemeDark);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjects_select);

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(REPORTS)) {
            isReports = true;
            if (extras.containsKey(DATETIME)) {
                isDate = true;
                dateTime = extras.getString(DATETIME);
            }
        }

        initPreferences();

        RecyclerView recyclerView = findViewById(R.id.recyclerview_subjects);
        adapter = new SubjectListAdapter(this);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager l = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(l);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                l.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        if (!isReports) {
            ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter, TOUCH_DRAG);
            ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
            touchHelper.attachToRecyclerView(recyclerView);
        }

        subjectViewModel = ViewModelProviders.of(this).get(SubjectViewModel.class);
        subjectViewModel.recalculatePositions();
        startObserving();
        subjectViewModel.getMaxPos().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer pos) {
                //adapter.setMaxPos(pos);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SubjectsSelectActivity.this, NewSubjectActivity.class);
                startActivityForResult(intent, NEW_SUBJECT_ACTIVITY_REQUEST_CODE);
            }
        });

        adapter.setOnItemClickListener(new SubjectListAdapter.ClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Subject subject = adapter.getSubjectAtPosition(position);
                if (isReports) {
                    launchNewReportActivity(subject);
                    //} else {
                    //    launchUpdateSubjectActivity(subject);
                }
                Log.d(LOG_TAG, "selected subject: " + subject.toString());
            }

            @Override
            public void onItemEditClick(View v, int position) {
                Subject subject = adapter.getSubjectAtPosition(position);
                launchUpdateSubjectActivity(subject);
                Log.d(LOG_TAG, "EDIT selected subject: " + subject.toString());
            }
        });
    }

    private void startObserving() {
        if (subjectViewModel != null && !subjectViewModel.getAllSubjects().hasObservers()) {
            Log.d(LOG_TAG, "start observing");
            subjectViewModel.getAllSubjects().observe(this, new Observer<List<Subject>>() {
                @Override
                public void onChanged(@Nullable final List<Subject> subjects) {
                    // Update the cached copy of the words in the adapter.
                    Log.d(LOG_TAG, "observer callback");
                    adapter.setSubjects(subjects);
                }
            });
        }
    }

    public void stopObserving() {
        if (subjectViewModel != null && subjectViewModel.getAllSubjects().hasObservers()) {
            Log.d(LOG_TAG, "stop observing");
            subjectViewModel.getAllSubjects().removeObservers(this);
        }
    }

    private void launchUpdateSubjectActivity(Subject subject) {
        Intent intent = new Intent(this, NewSubjectActivity.class);
        intent.putExtra(TITLE, subject.title);
        intent.putExtra(DESCRIPTION, subject.description);
        intent.putExtra(COLOR, subject.color);
        intent.putExtra(ID, subject.id);
        intent.putExtra(POS, subject.pos);
        Log.d(LOG_TAG, "id=" + str(subject.id));
        startActivityForResult(intent, UPDATE_SUBJECT_ACTIVITY_REQUEST_CODE);
    }


    private void launchNewReportActivity(Subject subject) {
        Intent intent = new Intent(this, NewReportActivity.class);
        intent.putExtra(SUBJECT, subject.title);
        intent.putExtra(COLOR, subject.color);
        if (isDate) {
            intent.putExtra(DATETIME, dateTime);
        }
        Log.d(LOG_TAG, "New report for subject " + subject.toString());
        startActivityForResult(intent, NEW_REPORT_ACTIVITY_REQUEST_CODE);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case NEW_REPORT_ACTIVITY_REQUEST_CODE:
                setResult(resultCode, data);  // send back to main activity
                finish();
                break;
            case NEW_SUBJECT_ACTIVITY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    String title = data.getStringExtra(TITLE);
                    String description = data.getStringExtra(DESCRIPTION);
                    String color = data.getStringExtra(COLOR);
                    Integer pos = subjectViewModel.getMaxPos().getValue() + 1;
                    Log.d(LOG_TAG, "new subject: pos=" + str(pos));
                    Subject subject = new Subject(title, description, color, pos);
                    subjectViewModel.insert(subject);
                } else {
                    Toast.makeText(getApplicationContext(), R.string.empty_not_saved, Toast.LENGTH_LONG).show();
                }
                break;
            case UPDATE_SUBJECT_ACTIVITY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    int id = data.getIntExtra(ID, -1);
                    if (data.getExtras().containsKey(DELETE)) {
                        subjectViewModel.delete(id);
                    } else {
                        String title = data.getStringExtra(TITLE);
                        String description = data.getStringExtra(DESCRIPTION);
                        String color = data.getStringExtra(COLOR);
                        int pos = data.getIntExtra(POS, 0);
                        Subject subject = new Subject(id, title, description, color, pos);
                        if (id != -1) {
                            subjectViewModel.update(subject);
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.error_updating_subject, Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.empty_not_saved, Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public void swapSubjectPos(int from, int to) {
        //subjectViewModel.swapPos(from, to);
        final Swap swap = new Swap(this);
        Log.d(LOG_TAG, "swap started: " + str(from) + " -> " + str(to));
        swap.execute(from, to);
    }

    private void initPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        //sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        String appHeader = sharedPreferences.getString(PREF_APP_HEADER, getString(R.string.app_name));
        String header = this.getString(R.string.title_select_subjects, appHeader);
        setTitle(header);
    }


    private static class Swap extends AsyncTask<Integer, Void, Void> {
        private final WeakReference<SubjectsSelectActivity> activityReference;

        // only retain a weak reference to the activity
        Swap(SubjectsSelectActivity context) {
            activityReference = new WeakReference<>(context);
        }

        @Override
        protected Void doInBackground(Integer... params) {
            int from = params[0];
            int to = params[1];
            // get a reference to the activity if it is still there
            SubjectsSelectActivity activity = activityReference.get();
            if (activity == null || activity.isFinishing()) return null;
            activity.subjectViewModel.swapPosSync(from, to);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d(LOG_TAG, "swap complete");
            SubjectsSelectActivity activity = activityReference.get();
            if (activity == null || activity.isFinishing()) return;
            activity.startObserving();
        }
    }


}
