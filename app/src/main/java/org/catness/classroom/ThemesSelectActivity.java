package org.catness.classroom;

import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.catness.classroom.database.DeleteTheme;
import org.catness.classroom.database.ImportTheme;
import org.catness.classroom.themes.Theme;
import org.catness.classroom.themes.ThemeListAdapter;
import org.catness.classroom.themes.ThemeViewModel;

import java.util.List;

import static org.catness.classroom.utils.Constants.ACTIVE;
import static org.catness.classroom.utils.Constants.COMMENT;
import static org.catness.classroom.utils.Constants.COUNT;
import static org.catness.classroom.utils.Constants.DELETE;
import static org.catness.classroom.utils.Constants.ID;
import static org.catness.classroom.utils.Constants.NAME;
import static org.catness.classroom.utils.Constants.PATH;
import static org.catness.classroom.utils.Constants.PREF_APP_HEADER;
import static org.catness.classroom.utils.Constants.PREF_THEME;
import static org.catness.classroom.utils.Constants.THEME_DEFAULT;
import static org.catness.classroom.utils.Constants.UPDATE_THEME_ACTIVITY_REQUEST_CODE;
import static org.catness.classroom.utils.Utils.str;

public class ThemesSelectActivity extends AppCompatActivity {
    private static final String LOG_TAG = ThemesSelectActivity.class.getSimpleName();
    private ThemeViewModel themeViewModel;
    private ThemeListAdapter adapter;
    private int selectedTheme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (((App) getApplicationContext()).isNightModeEnabled())
            setTheme(R.style.ActivityThemeDark);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_themes_select);
        initPreferences();

        RecyclerView recyclerView = findViewById(R.id.recyclerview_themes);
        adapter = new ThemeListAdapter(this, selectedTheme);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager l = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(l);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                l.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        themeViewModel = ViewModelProviders.of(this).get(ThemeViewModel.class);
        themeViewModel.getAllThemes().observe(this, new Observer<List<Theme>>() {
            @Override
            public void onChanged(@Nullable final List<Theme> themes) {
                adapter.setThemes(themes);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showUploadConfirmationDialog();
            }
        });

        adapter.setOnItemClickListener(new ThemeListAdapter.ClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Theme theme = adapter.getThemeAtPosition(position);
                launchUpdateThemeActivity(theme);
            }
        });
    }

    private void launchUpdateThemeActivity(Theme theme) {
        Intent intent = new Intent(this, NewThemeActivity.class);
        Log.d(LOG_TAG, "launch update new theme : " + theme.toString());
        intent.putExtra(ID, theme.id);
        intent.putExtra(NAME, theme.name);
        intent.putExtra(COMMENT, theme.comment);
        intent.putExtra(COUNT, theme.count);
        intent.putExtra(PATH, theme.path);
        intent.putExtra(ACTIVE, theme.id == selectedTheme);
        startActivityForResult(intent, UPDATE_THEME_ACTIVITY_REQUEST_CODE);
    }


    private void initPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        //sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        String appHeader = sharedPreferences.getString(PREF_APP_HEADER, getString(R.string.app_name));
        String header = this.getString(R.string.title_select_themes);
        setTitle(header);
        selectedTheme = sharedPreferences.getInt(PREF_THEME, THEME_DEFAULT);
        Log.d(LOG_TAG, "got selected theme : " + selectedTheme);
    }

    private void updatePreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(PREF_THEME, selectedTheme);
        editor.apply();
        Log.d(LOG_TAG, "set selected theme to : " + selectedTheme);

        selectedTheme = sharedPreferences.getInt(PREF_THEME, THEME_DEFAULT);
        Log.d(LOG_TAG, "##### got selected theme : " + selectedTheme);


        adapter.setSelectedTheme(selectedTheme);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case UPDATE_THEME_ACTIVITY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    int id = data.getIntExtra(ID, -1);
                    if (data.getExtras().containsKey(DELETE)) {
                        if (id != THEME_DEFAULT) {
                            DeleteTheme deleteTheme = new DeleteTheme(this, id);
                            deleteTheme.execute();
                            if (id == selectedTheme) {
                                selectedTheme = THEME_DEFAULT;
                                updatePreferences();
                            }
                        }
                    } else {
                        String name = data.getStringExtra(NAME);
                        String path = data.getStringExtra(PATH);
                        String comment = data.getStringExtra(COMMENT);
                        Integer count = data.getIntExtra(COUNT, 0);
                        boolean isActive = data.getBooleanExtra(ACTIVE, false);
                        Theme theme = new Theme(id, name, path, count, comment);
                        Log.d(LOG_TAG, "updating theme : " + theme.toString() + " isActive=" + str(isActive));
                        themeViewModel.update(theme);
                        if (isActive && (id != selectedTheme)) {
                            selectedTheme = id;
                            updatePreferences();
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.empty_not_saved, Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void showUploadConfirmationDialog() {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the positive and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.upload_theme);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                uploadTheme();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Cancel" button, so dismiss the dialog
                // and continue editing the pet.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void uploadTheme() {
        ImportTheme importTheme = new ImportTheme(this);
        importTheme.execute();
    }
}
