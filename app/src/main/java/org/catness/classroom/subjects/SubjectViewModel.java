package org.catness.classroom.subjects;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import org.catness.classroom.database.DataRepository;

import java.util.List;

public class SubjectViewModel extends AndroidViewModel {
    private final DataRepository repository;
    private final LiveData<List<Subject>> subjects;
    private final LiveData<Integer> maxpos;

    public SubjectViewModel(@NonNull Application application) {
        super(application);
        repository = new DataRepository(application);
        subjects = repository.getAllSubjects();
        maxpos = repository.getMaxPos();
    }

    public LiveData<List<Subject>> getAllSubjects() {
        return subjects;
    }

    public LiveData<Integer> getMaxPos() {
        return maxpos;
    }

    public void deleteAll() {
        repository.deleteAllSubjects();
    }

    public void insert(Subject subject) {
        repository.insertSubject(subject);
    }

    public void delete(int id) {
        repository.deleteSubject(id);
    }

    public void update(Subject subject) {
        repository.updateSubject(subject);
    }

    public void updateSubjectPos(int id, int pos) {
        repository.updateSubjectPos(id, pos);
    }

    public void swapPosSync(int from, int to) {
        repository.swapSubjectPosSync(from, to);
    }

    public void recalculatePositions() {
        repository.recalculateSubjectPositions();
    }
}
