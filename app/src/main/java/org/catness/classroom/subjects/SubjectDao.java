package org.catness.classroom.subjects;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface SubjectDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Subject subject);

    @Query("select * from subjects order by pos asc")
    LiveData<List<Subject>> getAllSubjects();

    @Query("select * from subjects order by pos asc")
    List<Subject> getAllSubjectsSync();

    @Query("delete from subjects where id=:id")
    void delete(int id);

    @Query("DELETE FROM subjects")
    void deleteAll();

    @Update
    void update(Subject... subject);

    @Query("update subjects set pos=:pos where id=:id")
    void updatePos(int id, int pos);

    @Query("update subjects set pos=:pos_tmp where pos=:pos")
    void updatePosStart(int pos, int pos_tmp);

    @Query("update subjects set pos=:pos where pos=:pos_tmp")
    void updatePosEnd(int pos, int pos_tmp);

    @Query("update subjects set pos=pos-1 where pos>:from and pos<=:to")
    void updatePosMoveUp(int from, int to);

    @Query("update subjects set pos=pos+1 where pos>=:to and pos<:from")
    void updatePosMoveDown(int from, int to);

    @Query("select count(*) from subjects")
    int getCount();

// --Commented out by Inspection START (5/14/19 4:25 PM):
//    @Query("select max(pos) from subjects")
//    int getMaxPosSimple();
// --Commented out by Inspection STOP (5/14/19 4:25 PM)

    @Query("select max(pos) from subjects")
    LiveData<Integer> getMaxPos();

}
