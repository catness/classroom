package org.catness.classroom.subjects;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import static org.catness.classroom.utils.Utils.str;

@Entity(tableName = "subjects")
public class Subject {
    private final static String LOG_TAG = Subject.class.getSimpleName();
    @PrimaryKey(autoGenerate = true)
    public int id;

    @NonNull
    //@ColumnInfo(name = "title")
    public String title;
    public String description;
    public String color;
    public int pos;

    public Subject(@NonNull String title, String description, String color, int pos) {
        // this one creates a new word with a new ID
        this.title = title;
        this.description = description;
        this.color = color;
        this.pos = pos;
        //Log.d(LOG_TAG," new subject: " + toString());
    }

    @Ignore
    public Subject(int id, @NonNull String title, String description, String color, int pos) {
        // To update an existing item, create the item using this constructor.
        // Room will use the primary key (in this case the id)
        // to find the existing entry in the database so it can be updated.
        // https://codelabs.developers.google.com/codelabs/android-training-room-delete-data/#8
        this.id = id;
        this.title = title;
        this.description = description;
        this.color = color;
        this.pos = pos;
    }

    @Ignore
    public Subject() {
    }


    public String toString() {
        return "Subject: " + title + " : " + description + " : " + color + " : " + " id=" + str(id) + " pos=" + str(pos);
    }
}
