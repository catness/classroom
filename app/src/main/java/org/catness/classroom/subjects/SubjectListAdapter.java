package org.catness.classroom.subjects;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.catness.classroom.R;
import org.catness.classroom.SubjectsSelectActivity;
import org.catness.classroom.utils.ItemTouchHelperAdapter;

import java.util.Collections;
import java.util.List;

import static org.catness.classroom.utils.Constants.POS_TMP;
import static org.catness.classroom.utils.Utils.getContrastColor;
import static org.catness.classroom.utils.Utils.str;

public class SubjectListAdapter extends RecyclerView.Adapter<SubjectListAdapter.SubjectViewHolder> implements ItemTouchHelperAdapter {
    private final static String LOG_TAG = SubjectListAdapter.class.getSimpleName();
    private final LayoutInflater mInflater;
    private List<Subject> subjects; // Cached copy of subjects
    private static ClickListener clickListener;
    private final SubjectsSelectActivity context;

    public SubjectListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        this.context = (SubjectsSelectActivity) context;
        Log.d(LOG_TAG, "constructor of SubjectListAdapter");
    }

    @NonNull
    @Override
    public SubjectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(LOG_TAG, "onCreateViewHolder");
        View itemView = mInflater.inflate(R.layout.subject_item, parent, false);
        return new SubjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SubjectViewHolder holder, int position) {
        if (subjects != null) {
            Subject current = subjects.get(position);
            holder.titleView.setText(current.title);
            Log.d(LOG_TAG, "onBindViewHolder: " + current.toString() + " position: " + str(position));
            try {
                int bg = Color.parseColor(current.color);
                holder.itemView.setBackgroundColor(bg);
                int fg = getContrastColor(bg);
                holder.titleView.setTextColor(fg);
            } catch (Exception e) {
                Log.d(LOG_TAG, "invalid color " + current.color);
            }
        }
        /* else {
            // Covers the case of data not being ready yet.
            // holder.titleView.setText("No Subject");
        } */
    }

    public void setSubjects(List<Subject> params) {
        Log.v(LOG_TAG, "------------------ set subjects");
        Subject s = params.get(0);
        if (s.pos == POS_TMP) {
            Log.v(LOG_TAG, s.toString() + " ..... swap is not complete... ignoring");
            return;
        }
        subjects = params;
        notifyDataSetChanged();
    }
/*
    public void setMaxPos(Integer maxpos) {
        Integer maxpos1 = maxpos;
        notifyDataSetChanged();
    }
*/

    // getItemCount() is called many times, and when it is first called,
    // subjects has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (subjects != null)
            return subjects.size();
        else return 0;
    }

    public Subject getSubjectAtPosition(int position) {
        return subjects.get(position);
    }

    private void swapSubjectsLocal(int from, int to) {
        Subject s1 = subjects.get(from);
        Subject s2 = subjects.get(to);
        Log.d(LOG_TAG, "swap subjects local " + s1.toString() + " -> " + s2.toString());
        subjects.get(from).pos = to;
        subjects.get(to).pos = from;

        Collections.swap(subjects, from, to);

        s1 = subjects.get(from);
        s2 = subjects.get(to);
        Log.d(LOG_TAG, "swap subjects local result : " + s1.toString() + " ... " + s2.toString());
    }


    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        context.stopObserving();
        Log.d(LOG_TAG, "Item moved from " + str(fromPosition) + " to " + str(toPosition));
        swapSubjectsLocal(fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
    }

    public void reallyMoved(int fromPosition, int toPosition) {
        Log.d(LOG_TAG, "Really moved! " + str(fromPosition) + " to " + str(toPosition));
        context.swapSubjectPos(fromPosition, toPosition);
    }

    @Override
    public void onItemDismiss(int direction) {
    }

    public interface ClickListener {
        void onItemClick(View v, int position);

        void onItemEditClick(View v, int position);
    }


    public void setOnItemClickListener(ClickListener clickListener) {
        SubjectListAdapter.clickListener = clickListener;
    }


    class SubjectViewHolder extends RecyclerView.ViewHolder {
        private final TextView titleView;
        private final ImageView edit;

        private SubjectViewHolder(View itemView) {
            super(itemView);
            titleView = itemView.findViewById(R.id.titleView);
            edit = itemView.findViewById(R.id.edit);


            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onItemEditClick(view, getAdapterPosition());
                }
            });


            // this listener sends the clicks to the parent activity where they are processed
            // at adapter.setOnItemClickListener
            // or something. anyway without this part the clicks are not processed
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onItemClick(view, getAdapterPosition());
                }
            });

        }
    }

}
