package org.catness.classroom;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;

import org.catness.classroom.reports.Report;
import org.catness.classroom.reports.ReportDayListAdapter;
import org.catness.classroom.utils.OnSwipeTouchListener;
import org.catness.classroom.utils.SimpleItemTouchHelperCallback;
import org.catness.classroom.utils.TouchTextView;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static android.view.View.GONE;
import static org.catness.classroom.utils.Constants.DATETIME;
import static org.catness.classroom.utils.Constants.DAY;
import static org.catness.classroom.utils.Constants.MONTH;
import static org.catness.classroom.utils.Constants.NEW_REPORT_ACTIVITY_REQUEST_CODE;
import static org.catness.classroom.utils.Constants.PREF_APP_HEADER;
import static org.catness.classroom.utils.Constants.PREF_SOUND;
import static org.catness.classroom.utils.Constants.REPORTS;
import static org.catness.classroom.utils.Constants.SWIPE_LEFT;
import static org.catness.classroom.utils.Constants.SWIPE_RIGHT;
import static org.catness.classroom.utils.Constants.TOUCH_SWIPE;
import static org.catness.classroom.utils.Constants.YEAR;
import static org.catness.classroom.utils.Utils.endDay;
import static org.catness.classroom.utils.Utils.startDay;
import static org.catness.classroom.utils.Utils.str;

public class ReportsDayActivity extends ReportsActivityCommon {
    private final String LOG_TAG = ReportsDayActivity.class.getSimpleName();
    private TouchTextView noReports;
    private Long start, end;
    private ReportDayListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (((App) getApplicationContext()).isNightModeEnabled())
            setTheme(R.style.ActivityThemeDark);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports_day);

        initPreferences();
        start = end = 0L;
        Bundle extras = getIntent().getExtras();
        year = extras.getInt(YEAR, 0);
        month = extras.getInt(MONTH, 0);
        day = extras.getInt(DAY, 0);

        noReports = findViewById(R.id.noReports);
        noReports.setOnTouchListener(new OnSwipeTouchListener(this) {
            public void onSwipeRight() {
                swipe(SWIPE_RIGHT);
            }

            public void onSwipeLeft() {
                swipe(SWIPE_LEFT);
            }
        });
        init();

        adapter = new ReportDayListAdapter(this);
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new ReportDayListAdapter.ClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Report report = adapter.getReportAtPosition(position);
                launchUpdateReportActivity(report);
            }
        });
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter, TOUCH_SWIPE);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);
        adapter.setOnSwipeClickListener(new ReportDayListAdapter.SwipeListener() {
            @Override
            public void onSwipe(int direction) {
                swipe(direction);
            }
        });

        FloatingActionButton fab = this.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ReportsDayActivity.this, SubjectsSelectActivity.class);
                intent.putExtra(REPORTS, true);
                intent.putExtra(DATETIME, dateText + " 12:00");
                startActivityForResult(intent, NEW_REPORT_ACTIVITY_REQUEST_CODE);
            }
        });

        initDate(year, month, day);
        isDay = true;
    }

    private void swipe(int direction) {
        Log.d(LOG_TAG, "swipe " + str(direction));
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        switch (direction) {
            case SWIPE_LEFT:
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                break;
            case SWIPE_RIGHT:
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                break;
            default:
                return;
        }
        initDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }

    private void initDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
        dateText = String.format(Locale.ENGLISH, "%04d-%02d-%02d", year, month, day);
        pointsDayView.setText(dateText);
        Log.d(LOG_TAG, "init date text: =" + dateText + "=");
        if (start != 0L) reportViewModel.getAllReportsDay(start, end).removeObservers(this);
        start = startDay(year, month, day);
        end = endDay(year, month, day);
        reportViewModel.getAllReportsDay(start, end).observe(this, new Observer<List<Report>>() {
            @Override
            public void onChanged(@Nullable final List<Report> reports) {
                // Update the cached copy of the words in the adapter.
                Log.d(LOG_TAG, "onChanged: reports " + reports.size());
                adapter.setReports(reports);
                if (reports.size() == 0) {
                    recyclerView.setVisibility(GONE);
                    noReports.setVisibility(View.VISIBLE);
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    noReports.setVisibility(GONE);
                }
            }
        });

        final GetDayPoints getDayPoints = new GetDayPoints(this);
        getDayPoints.execute(year, month, day);
    }


    private void initPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        //sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        String appHeader = sharedPreferences.getString(PREF_APP_HEADER, getString(R.string.app_name));
        String header = this.getString(R.string.title_reports_day, appHeader);
        setTitle(header);
        isSound = sharedPreferences.getBoolean(PREF_SOUND, true);
    }

    private void returnToCalendar() {
        Intent replyIntent = new Intent();
        replyIntent.putExtra(YEAR, year);
        replyIntent.putExtra(MONTH, month);
        setResult(RESULT_OK, replyIntent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        Log.d(LOG_TAG, "Navigate up!");
        returnToCalendar();
        finish();
        //return super.onSupportNavigateUp();
        return true;
    }

    @Override
    public void onBackPressed() {
        Log.d(LOG_TAG, "Back pressed!");
        returnToCalendar();
        finish();
        //super.onBackPressed();
    }
}
