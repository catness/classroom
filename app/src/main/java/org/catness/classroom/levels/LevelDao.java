package org.catness.classroom.levels;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

@Dao
public interface LevelDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Level level);

    @Query("select * from level limit 1")
    LiveData<Level> getLevel();

    @Query("select * from level limit 1")
    Level getLevelSimple();

    @Update
    void update(Level level);

    @Query("select count(*) from level")
    int getCount();

}
