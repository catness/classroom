package org.catness.classroom.levels;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.catness.classroom.R;
import org.catness.classroom.database.SchoolDatabase;
import org.catness.classroom.reports.ReportViewModel;
import org.catness.classroom.rewards.RewardViewModel;
import org.catness.classroom.themes.Theme;
import org.catness.classroom.themes.ThemeDao;
import org.catness.classroom.themes.ThemeLevel;
import org.catness.classroom.themes.ThemeLevelDao;
import org.catness.classroom.utils.Utils;

import java.io.File;
import java.lang.ref.WeakReference;

import static org.catness.classroom.utils.Constants.DATABASE_NAME;
import static org.catness.classroom.utils.Constants.MAXPOINTS;
import static org.catness.classroom.utils.Constants.PREF_THEME;
import static org.catness.classroom.utils.Constants.THEME_DEFAULT;
import static org.catness.classroom.utils.Utils.endDay;
import static org.catness.classroom.utils.Utils.startDay;
import static org.catness.classroom.utils.Utils.str;

public class LevelShow {
    private static final String LOG_TAG = LevelShow.class.getSimpleName();
    private final ProgressBar levelBar;
    private final TextView levelTitle;
    private final TextView levelPoints;
    private final TextView dayPoints;
    private final ImageView levelImage;
    private final LevelViewModel levelViewModel;
    private final ReportViewModel reportViewModel;
    private final AppCompatActivity context;
    private int currentLevel = -1;
    private int maxpoints = MAXPOINTS;

    private final String[] titles = {
            "The Fool",
            "The Magician",
            "The High Priestess",
            "The Empress",
            "The Emperor",
            "The Hierophant",
            "The Lovers",
            "The Chariot",
            "Justice",
            "The Hermit",
            "Wheel of Fortune",
            "Strength",
            "The Hanged Man",
            "Death",
            "Temperance",
            "The Devil",
            "The Tower",
            "The Star",
            "The Moon",
            "The Sun",
            "Judgement",
            "The World",   // 21

            "2 of Cups",
            "3 of Cups",
            "4 of Cups",
            "5 of Cups",
            "6 of Cups",
            "7 of Cups",
            "8 of Cups",
            "9 of Cups",
            "10 of Cups",
            "Page of Cups",
            "Knight of Cups",
            "Queen of Cups",
            "King of Cups",
            "Ace of Cups",   // 35

            "2 of Pentacles",
            "3 of Pentacles",
            "4 of Pentacles",
            "5 of Pentacles",
            "6 of Pentacles",
            "7 of Pentacles",
            "8 of Pentacles",
            "9 of Pentacles",
            "10 of Pentacles",
            "Page of Pentacles",
            "Knight of Pentacles",
            "Queen of Pentacles",
            "King of Pentacles",
            "Ace of Pentacles",    // 49

            "2 of Wands",
            "3 of Wands",
            "4 of Wands",
            "5 of Wands",
            "6 of Wands",
            "7 of Wands",
            "8 of Wands",
            "9 of Wands",
            "10 of Wands",
            "Page of Wands",
            "Knight of Wands",
            "Queen of Wands",
            "King of Wands",
            "Ace of Wands",   // 63

            "2 of Swords",
            "3 of Swords",
            "4 of Swords",
            "5 of Swords",
            "6 of Swords",
            "7 of Swords",
            "8 of Swords",
            "9 of Swords",
            "10 of Swords",
            "Page of Swords",
            "Knight of Swords",
            "Queen of Swords",
            "King of Swords",
            "Ace of Swords"   // 77

    };

    public LevelShow(final AppCompatActivity context) {
        this.context = context;
        levelBar = context.findViewById(R.id.levelBar);
        levelBar.setMax(maxpoints);

        levelViewModel = ViewModelProviders.of(context).get(LevelViewModel.class);
        levelViewModel.getLevel().observe(context, new Observer<Level>() {
            @Override
            public void onChanged(@Nullable final Level level) {
                update(level);
            }
        });
        reportViewModel = ViewModelProviders.of(context).get(ReportViewModel.class);

        levelTitle = context.findViewById(R.id.levelTitle);
        levelPoints = context.findViewById(R.id.levelPoints);
        levelImage = context.findViewById(R.id.levelImage);
        dayPoints = context.findViewById(R.id.dayPoints);

        todayPoints();
    }

    private void todayPoints() {
        final GetTodayPoints getDayPoints = new GetTodayPoints(context, this);
        getDayPoints.execute();
    }


    public void updateDayPoints(int points) {
        Log.d(LOG_TAG, "updateDayPoints: " + str(points));
        dayPoints.setText(str(points));
    }

    private void update(Level level) {
        if (level == null) return;
        Log.d(LOG_TAG, "update level : " + level.toString());
        int points = level.points;
        int num = level.num;
        levelBar.setProgress(points);
        String out = context.getString(R.string.level_points, points, maxpoints);
        levelPoints.setText(out);
        int lastLevel = -1;
        if (num != lastLevel) {
            currentLevel = num;
            updateThemeInfo();
        }
    }

    public void updateThemeInfo() {
        if (currentLevel == -1) return;
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int selectedTheme = sharedPreferences.getInt(PREF_THEME, THEME_DEFAULT);
        Log.d(LOG_TAG, "selected theme : " + selectedTheme);
        if (selectedTheme == THEME_DEFAULT) {
            try {
                String title = titles[currentLevel];
                levelTitle.setText(title);
                setImage(currentLevel);
            } catch (Exception e) {
                // in case the number is too high and there is no level title for it yet
                levelTitle.setText(R.string.max_level);
                setImage(78); // the last image (the card back)
            }
        } else {
            SetLevelAsync setLevelAsync = new SetLevelAsync(context, this, selectedTheme, currentLevel);
            setLevelAsync.execute();
        }
    }

    public boolean isLevelUp(int points) {
        try {
            return levelViewModel.getLevel().getValue().isLevelUp(points, maxpoints);
        } catch (Exception e) {
            return false;
        }
    }

    public void reset() {
        levelViewModel.update(new Level(0, 0, 0));
    }

    public void updateMaxpoints(int maxpoints) {
        this.maxpoints = maxpoints;
        levelBar.setMax(maxpoints);
        update(levelViewModel.getLevel().getValue());
    }

    private void setImage(int level) {
        String uri = "@drawable/tarot" + str(level);  // where myresource (without the extension) is the file
        int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
        Drawable res = context.getDrawable(imageResource);
        levelImage.setImageDrawable(res);
    }

    private void setLevelInfo(Bitmap bitmap, String title) {
        levelTitle.setText(title);
        if (bitmap != null) {
            levelImage.setImageBitmap(bitmap);
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            levelImage.setMinimumWidth(width);
            levelImage.setMinimumHeight(height);
        }
    }

    public void levelUpDialog(RewardViewModel rewardViewModel) {
        LayoutInflater inflater = context.getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.levelup, (ViewGroup) null);
        // casting null to ViewGroup fixes the lint warning about not passing null as a layout parent
        TextView messageView = alertLayout.findViewById(R.id.levelupMessage);
        TextView rewardView = alertLayout.findViewById(R.id.levelupReward);

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(alertLayout);
        builder.setCancelable(true);
        //builder.setTitle("Level up");
        String reward = rewardViewModel.selectRandomReward();
        //Spanned message = Html.fromHtml("Congrats with " + "<b>" + "level up!" + "</b> Your reward:");
        Spanned message = Html.fromHtml(context.getString(R.string.congrats_levelup));
        messageView.setText(message);
        rewardView.setText(reward);

        final AlertDialog dialog = builder.create();
        dialog.show(); //show() should be called before dialog.getButton().

        Button ok = alertLayout.findViewById(R.id.levelupOK);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }


    private static class LevelResult {
        final Bitmap image;
        final String name;

        LevelResult(Bitmap image, String name) {
            this.image = image;
            this.name = name;
        }
    }


    private static class SetLevelAsync extends AsyncTask<Void, Void, LevelResult> {
        private final WeakReference<AppCompatActivity> activityReference;
        private final WeakReference<LevelShow> contextReferenceThis;
        private final int themeID;
        private int level;

        SetLevelAsync(AppCompatActivity context, LevelShow contextThis, int themeID, int level) {
            activityReference = new WeakReference<>(context);
            contextReferenceThis = new WeakReference<>(contextThis);
            this.themeID = themeID;
            this.level = level;
        }

        @Override
        protected LevelResult doInBackground(Void... voids) {
            AppCompatActivity context = activityReference.get();
            if (context == null) return null;
            SchoolDatabase db = SchoolDatabase.getDatabase(context.getApplication());
            ThemeDao themeDao = db.themeDao();
            Theme theme = themeDao.get(themeID);
            if (level >= theme.count) level = theme.count - 1;
            ThemeLevelDao themeLevelDao = db.themeLevelDao();
            ThemeLevel themeLevel = themeLevelDao.getThemeLevel(themeID, level);

            try {
                File fileDir = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), DATABASE_NAME);
                File image = new File(fileDir, theme.path + "/" + themeLevel.image);
                Log.d(LOG_TAG, "image for level " + themeLevel.name + " : " + image.getAbsolutePath());
                Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath());
                DisplayMetrics metrics = new DisplayMetrics();
                context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
                float scaleWidth = metrics.scaledDensity;
                float scaleHeight = metrics.scaledDensity;
                Log.d(LOG_TAG, "scale density: " + scaleWidth + "," + scaleHeight);
                Matrix matrix = new Matrix();
                matrix.postScale(scaleWidth, scaleHeight);
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                Log.d(LOG_TAG, "bitmap width=" + bitmap.getWidth() + " height=" + bitmap.getHeight());
                return new LevelResult(bitmap, themeLevel.name);
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage());
                return new LevelResult(null, themeLevel.name);
            }
        }

        @Override
        protected void onPostExecute(LevelResult levelResult) {
            LevelShow context = contextReferenceThis.get();
            if (context == null) return;
            if (levelResult == null) return;
            context.setLevelInfo(levelResult.image, levelResult.name);
        }
    }


    private static class GetTodayPoints extends AsyncTask<Void, Void, Integer> {
        private final WeakReference<AppCompatActivity> activityReference;
        private final WeakReference<LevelShow> contextReferenceThis;

        GetTodayPoints(AppCompatActivity context, LevelShow contextThis) {
            activityReference = new WeakReference<>(context);
            contextReferenceThis = new WeakReference<>(contextThis);
        }

        @Override
        protected Integer doInBackground(Void... params) {

            AppCompatActivity context = activityReference.get();
            SchoolDatabase db = SchoolDatabase.getDatabase(context.getApplication());
            final LevelDao levelDao = db.levelDao();
            if (levelDao.getCount() == 0) { // database not inited yet
                return Integer.MIN_VALUE;
            }
            String today = Utils.strDateToday();
            int year = Integer.parseInt(today.substring(0, 4));
            int month = Integer.parseInt(today.substring(5, 7));
            int day = Integer.parseInt(today.substring(8, 10));
            Long start = startDay(year, month, day);
            Long end = endDay(year, month, day);
            Log.d(LOG_TAG, "getTodayPoints bg task: year=" + str(year) + " month=" + str(month) + " day=" + str(day) + " start=" + str(start) + " end=" + str(end));
            // get a reference to the activity if it is still there
            LevelShow contextThis = contextReferenceThis.get();
            if (contextThis == null) return 0;
            return contextThis.reportViewModel.getPointsDay(start, end);
        }

        @Override
        protected void onPostExecute(Integer points) {
            Log.d(LOG_TAG, "on Post execute: points=" + points);
            LevelShow context = contextReferenceThis.get();
            if (context == null) return;
            if (points == Integer.MIN_VALUE) {
                context.updateDayPoints(0);
                context.todayPoints();
            } else context.updateDayPoints(points);
        }
    }

}
