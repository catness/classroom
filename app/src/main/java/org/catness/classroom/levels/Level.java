package org.catness.classroom.levels;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import static org.catness.classroom.utils.Utils.str;

// (there's only one row in this table)

@Entity(tableName = "level")
public class Level {
    public int num, points;

    @PrimaryKey
    public int id;

    public Level(int id, int num, int points) {
        this.id = id;
        this.num = num;
        this.points = points;
    }

    @Ignore
    public Level() {
    }

    public void update(int add, int maxpoints) {
        points += add;
        if (points >= maxpoints) {
            num++;
            points -= maxpoints;
        }
    }

    public boolean isLevelUp(int add, int maxpoints) {
        return (points + add >= maxpoints);
    }

    public String toString() {
        return "Level: id=" + str(id) + " num=" + str(num) + " points=" + str(points);
    }
}
