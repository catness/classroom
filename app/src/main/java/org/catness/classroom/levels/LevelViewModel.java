package org.catness.classroom.levels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import org.catness.classroom.database.DataRepository;

public class LevelViewModel extends AndroidViewModel {
    private final DataRepository repository;
    private final LiveData<Level> level;

    public LevelViewModel(@NonNull Application application) {
        super(application);
        repository = new DataRepository(application);
        level = repository.getLevel();
    }

    public LiveData<Level> getLevel() {
        return level;
    }

    public void update(Level level) {
        repository.updateLevel(level);
    }

}
