package org.catness.classroom;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import org.catness.classroom.reports.ReportViewModel;
import org.catness.classroom.utils.CurrentDayDecorator;
import org.threeten.bp.YearMonth;

import java.lang.ref.WeakReference;
import java.util.Map;

import static org.catness.classroom.utils.Constants.CALENDAR_ACTIVITY_REQUEST_CODE;
import static org.catness.classroom.utils.Constants.DAY;
import static org.catness.classroom.utils.Constants.MONTH;
import static org.catness.classroom.utils.Constants.PREF_APP_HEADER;
import static org.catness.classroom.utils.Constants.YEAR;
import static org.catness.classroom.utils.Constants.paletteColors;
import static org.catness.classroom.utils.Utils.endDay;
import static org.catness.classroom.utils.Utils.startDay;
import static org.catness.classroom.utils.Utils.str;

// https://github.com/prolificinteractive/material-calendarview/wiki/Customization

public class CalendarActivity extends AppCompatActivity {
    private static final String LOG_TAG = CalendarActivity.class.getSimpleName();
    private MaterialCalendarView calendarView;
    private int month, year;
    private ReportViewModel reportViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (((App) getApplicationContext()).isNightModeEnabled())
            setTheme(R.style.ActivityCalendarThemeDark);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        calendarView = findViewById(R.id.calendarView);

        initPreferences();
        reportViewModel = ViewModelProviders.of(this).get(ReportViewModel.class);

        CalendarDay date = calendarView.getCurrentDate();
        recalculateDayPoints(date);

        calendarView.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
                recalculateDayPoints(date);
            }
        });

        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull final CalendarDay date, boolean selected) {
                Log.d(LOG_TAG, "date: " + date.toString());
                int year = date.getYear();
                int month = date.getMonth();
                int day = date.getDay();
                Intent intent = new Intent(CalendarActivity.this, ReportsDayActivity.class);
                intent.putExtra(YEAR, year);
                intent.putExtra(MONTH, month);
                intent.putExtra(DAY, day);
                startActivityForResult(intent, CALENDAR_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    private void recalculateDayPoints(CalendarDay date) {
        month = date.getMonth();
        year = date.getYear();
        Log.d(LOG_TAG, "recalculate points : date: " + date.toString() + " month=" + str(month) + " year=" + str(year));
        final GetDayPoints getDayPoints = new GetDayPoints(this);
        YearMonth yearMonthObject = YearMonth.of(year, month);
        int days = yearMonthObject.lengthOfMonth();
        //Log.d(LOG_TAG,"days in " + str(year) + "-" + str(month) + " : " + str(days));
        getDayPoints.execute(year, month, days);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(LOG_TAG, "back from activity : requestCode=" + str(requestCode));
        if (requestCode == CALENDAR_ACTIVITY_REQUEST_CODE) {
            int year = data.getIntExtra(YEAR, 0);
            int month = data.getIntExtra(MONTH, 0);
            Log.d(LOG_TAG, "got year : " + str(year) + " month : " + str(month));
            if ((year == 0 && month == 0) || (year == this.year && month == this.month)) return;
            this.year = year;
            this.month = month;
            CalendarDay date = CalendarDay.from(year, month, 1);
            calendarView.setCurrentDate(date);
        }
    }

    private static class GetDayPoints extends AsyncTask<Integer, Void, Integer[]> {
        // https://stackoverflow.com/questions/44309241/warning-this-asynctask-class-should-be-static-or-leaks-might-occur
        private final WeakReference<CalendarActivity> activityReference;

        // only retain a weak reference to the activity
        GetDayPoints(CalendarActivity context) {
            activityReference = new WeakReference<>(context);
        }

        @Override
        protected Integer[] doInBackground(Integer... params) {
            int year = params[0];
            int month = params[1];
            int days = params[2];
            Log.d(LOG_TAG, "getDayPoints: year=" + str(year) + " month=" + str(month) + " end=" + str(days));
            Integer[] points = new Integer[days];

            // get a reference to the activity if it is still there
            CalendarActivity activity = activityReference.get();
            if (activity == null || activity.isFinishing()) return points;

            for (int i = 1; i <= days; i++) {
                Long start = startDay(year, month, i);
                Long end = endDay(year, month, i);
                int daypoints = activity.reportViewModel.getPointsDay(start, end);
                //Log.d(LOG_TAG, "day=" + str(i) + " points: " + str(daypoints));
                points[i - 1] = daypoints;
            }
            //Integer[] points = {10,20,30,40,50};
            return points;
        }

        @Override
        protected void onPostExecute(Integer[] points) {
            // get a reference to the activity if it is still there
            CalendarActivity activity = activityReference.get();
            if (activity == null || activity.isFinishing()) return;

            for (int i = 0; i < points.length; i++) {
                int point = points[i];
                int d = i + 1;
                // Log.d(LOG_TAG, "day=" + str(d) + " points=" + str(point));
                try {
                    CalendarDay day = CalendarDay.from(activity.year, activity.month, d);
                    if (point > 0) {
                        for (Map.Entry<Integer, Integer> entry : paletteColors.entrySet()) {
                            int key = entry.getKey();
                            //Log.d(LOG_TAG, "colors: key=" + str(key));
                            if (point <= key) {
                                int color = entry.getValue();
                                // Log.d(LOG_TAG, "selected! color=" + str(color));
                                CurrentDayDecorator dec = new CurrentDayDecorator(color, day, activity);
                                activity.calendarView.addDecorator(dec);
                                break;
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.d(LOG_TAG, "exception " + e.getMessage());
                }
            }
        }

    }

    private void initPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        //sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        String appHeader = sharedPreferences.getString(PREF_APP_HEADER, getString(R.string.app_name));
        setTitle(appHeader);
    }

}

