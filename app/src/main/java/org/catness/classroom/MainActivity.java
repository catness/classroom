package org.catness.classroom;

import android.arch.lifecycle.Observer;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.catness.classroom.database.ExportDatabase;
import org.catness.classroom.database.ImportDatabase;
import org.catness.classroom.reports.Report;
import org.catness.classroom.reports.ReportListAdapter;
import org.catness.classroom.utils.Utils;

import java.util.List;

import static android.view.Menu.NONE;
import static org.catness.classroom.utils.Constants.EXPORT_EMAIL;
import static org.catness.classroom.utils.Constants.MAXPOINTS;
import static org.catness.classroom.utils.Constants.MENU_EXPORT;
import static org.catness.classroom.utils.Constants.MENU_IMPORT;
import static org.catness.classroom.utils.Constants.MENU_RESET;
import static org.catness.classroom.utils.Constants.MENU_THEME;
import static org.catness.classroom.utils.Constants.NEW_REPORT_ACTIVITY_REQUEST_CODE;
import static org.catness.classroom.utils.Constants.PREF_APP_HEADER;
import static org.catness.classroom.utils.Constants.PREF_EXPORT;
import static org.catness.classroom.utils.Constants.PREF_EXPORT_SHOW;
import static org.catness.classroom.utils.Constants.PREF_IMPORT_SHOW;
import static org.catness.classroom.utils.Constants.PREF_LIMIT;
import static org.catness.classroom.utils.Constants.PREF_MAXPOINTS;
import static org.catness.classroom.utils.Constants.PREF_NIGHT;
import static org.catness.classroom.utils.Constants.PREF_RESET;
import static org.catness.classroom.utils.Constants.PREF_SOUND;
import static org.catness.classroom.utils.Constants.PREF_THEME;
import static org.catness.classroom.utils.Constants.PREF_THEME_SHOW;
import static org.catness.classroom.utils.Constants.REPORTS;
import static org.catness.classroom.utils.Constants.REPORTS_LIMIT;
import static org.catness.classroom.utils.Constants.SELECT_THEME_REQUEST_CODE;
import static org.catness.classroom.utils.Utils.str;

public class MainActivity extends ReportsActivityCommon implements SharedPreferences.OnSharedPreferenceChangeListener {
    private final String LOG_TAG = MainActivity.class.getSimpleName();
    private boolean isReset = false, isExport = false, isImport = false, isTheme = false;  // show these options in the menu
    private String appHeader;
    private int exportMode;
    private int reportsLimit;
    private ReportListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final App app = (App) getApplicationContext();
        Log.d(LOG_TAG, app.getMyName());
        if (app.isNightModeEnabled()) {
            setTheme(R.style.ActivityThemeDark);
        }
        super.onCreate(savedInstanceState);

        Log.d(LOG_TAG, "onCreate");
        setContentView(R.layout.activity_main);

        init();
        initPreferences();
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        adapter = new ReportListAdapter(this);
        recyclerView.setAdapter(adapter);
        loadReports();
        adapter.setOnItemClickListener(new ReportListAdapter.ClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Report report = adapter.getReportAtPosition(position);
                launchUpdateReportActivity(report);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SubjectsSelectActivity.class);
                intent.putExtra(REPORTS, true);
                startActivityForResult(intent, NEW_REPORT_ACTIVITY_REQUEST_CODE);
            }
        });
        pointsToday();
        isDay = false;
        levelShow.updateThemeInfo();
    }

    public void pointsToday() {
        dateText = Utils.strDateToday();
        pointsDayView.setText(dateText);
        year = Integer.parseInt(dateText.substring(0, 4));
        month = Integer.parseInt(dateText.substring(5, 7));
        day = Integer.parseInt(dateText.substring(8, 10));
        //final GetDayPoints getDayPoints = new GetDayPoints();
        //getDayPoints.execute(year, month, day);
    }

    private void loadReports() {
        Log.d(LOG_TAG, "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~reports limit: " + str(reportsLimit));
        reportViewModel.getAllReports(reportsLimit).observe(this, new Observer<List<Report>>() {
            @Override
            public void onChanged(@Nullable final List<Report> reports) {
                // Update the cached copy of the words in the adapter.
                adapter.setReports(reports);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        if (isReset) menu.add(NONE, MENU_RESET, 900, R.string.action_reset);
        if (isExport) menu.add(NONE, MENU_EXPORT, 901, R.string.action_export);
        if (isImport) menu.add(NONE, MENU_IMPORT, 902, R.string.action_import);
        if (isTheme) menu.add(NONE, MENU_THEME, 903, R.string.action_themes);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;

        switch (id) {
            case R.id.action_settings:
                intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_subjects:
                intent = new Intent(MainActivity.this, SubjectsSelectActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_rewards:
                intent = new Intent(MainActivity.this, RewardsSelectActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_calendar:
                intent = new Intent(MainActivity.this, CalendarActivity.class);
                startActivity(intent);
                return true;
            case MENU_THEME:
                intent = new Intent(MainActivity.this, ThemesSelectActivity.class);
                startActivityForResult(intent, SELECT_THEME_REQUEST_CODE);
                return true;
            case MENU_EXPORT:
                final ExportDatabase exportDatabase = new ExportDatabase(this, exportMode, appHeader);
                exportDatabase.execute();
                return true;
            case MENU_IMPORT:
                final ImportDatabase importDatabase = new ImportDatabase(this);
                importDatabase.execute();
                return true;
            //case R.id.action_reset:
            case MENU_RESET:
                showResetConfirmationDialog();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private void resetLevels() {
        levelShow.reset();
    }

    private void showResetConfirmationDialog() {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the positive and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.reset_level);
        builder.setPositiveButton(R.string.reset, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                resetLevels();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Cancel" button, so dismiss the dialog
                // and continue editing the pet.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void initPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        isSound = sharedPreferences.getBoolean(PREF_SOUND, true);
        Log.d(LOG_TAG, "Init prefs, sound is: " + str(isSound));
        isReset = sharedPreferences.getBoolean(PREF_RESET, false);
        isExport = sharedPreferences.getBoolean(PREF_EXPORT_SHOW, false);
        isImport = sharedPreferences.getBoolean(PREF_IMPORT_SHOW, false);
        isTheme = sharedPreferences.getBoolean(PREF_THEME_SHOW, false);
        appHeader = sharedPreferences.getString(PREF_APP_HEADER, getString(R.string.app_name)).trim();
        setTitle(appHeader);
        exportMode = Integer.parseInt(sharedPreferences.getString(PREF_EXPORT, "0"));
        try {
            reportsLimit = Integer.parseInt(sharedPreferences.getString(PREF_LIMIT, str(REPORTS_LIMIT)));
        } catch (Exception e) {
            reportsLimit = REPORTS_LIMIT;
        }
        try {
            maxpoints = Integer.parseInt(sharedPreferences.getString(PREF_MAXPOINTS, str(MAXPOINTS)));
        } catch (Exception e) {
            maxpoints = MAXPOINTS;
        }
        levelShow.updateMaxpoints(maxpoints);

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.d(LOG_TAG, "\n\n\nshared pref changed key=" + key + "=");
        switch (key) {
            case PREF_SOUND:
                isSound = sharedPreferences.getBoolean(key, true);
                Log.d(LOG_TAG, "pref sound changed to: " + str(isSound));
                break;
            case PREF_RESET:
                isReset = sharedPreferences.getBoolean(key, false);
                invalidateOptionsMenu();
                break;
            case PREF_EXPORT_SHOW:
                isExport = sharedPreferences.getBoolean(key, false);
                invalidateOptionsMenu();
                break;
            case PREF_IMPORT_SHOW:
                isImport = sharedPreferences.getBoolean(key, false);
                invalidateOptionsMenu();
                break;
            case PREF_THEME_SHOW:
                isTheme = sharedPreferences.getBoolean(key, false);
                invalidateOptionsMenu();
                break;
            case PREF_APP_HEADER:
                appHeader = sharedPreferences.getString(key, getString(R.string.app_name)).trim();
                setTitle(appHeader);
                break;
            case PREF_EXPORT:
                exportMode = Integer.parseInt(sharedPreferences.getString(PREF_EXPORT, str(EXPORT_EMAIL)));
                Log.d(LOG_TAG, "export mode : " + str(exportMode));
                break;
            case PREF_NIGHT:
                boolean isNightMode = sharedPreferences.getBoolean(key, false);
                ((App) getApplicationContext()).setIsNightModeEnabled(isNightMode);
                recreate();
                break;
            case PREF_LIMIT:
                Log.d(LOG_TAG, "got reports limit");
                try {
                    reportsLimit = Integer.parseInt(sharedPreferences.getString(PREF_LIMIT, str(REPORTS_LIMIT)));
                } catch (Exception e) {
                    reportsLimit = REPORTS_LIMIT;
                }
                loadReports();
                break;

            case PREF_THEME:
                levelShow.updateThemeInfo();
                break;
            case PREF_MAXPOINTS:
                try {
                    maxpoints = Integer.parseInt(sharedPreferences.getString(PREF_MAXPOINTS, str(MAXPOINTS)));
                } catch (Exception e) {
                    maxpoints = MAXPOINTS;
                }
                levelShow.updateMaxpoints(maxpoints);
                break;
        }
    }

}
