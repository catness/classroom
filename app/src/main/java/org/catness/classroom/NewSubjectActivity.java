package org.catness.classroom;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.azeesoft.lib.colorpicker.ColorPickerDialog;

import static org.catness.classroom.utils.Constants.COLOR;
import static org.catness.classroom.utils.Constants.DELETE;
import static org.catness.classroom.utils.Constants.DESCRIPTION;
import static org.catness.classroom.utils.Constants.ID;
import static org.catness.classroom.utils.Constants.POS;
import static org.catness.classroom.utils.Constants.TITLE;
import static org.catness.classroom.utils.Utils.getContrastColor;
import static org.catness.classroom.utils.Utils.str;

public class NewSubjectActivity extends AppCompatActivity {
    private static final String LOG_TAG = NewSubjectActivity.class.getSimpleName();

    private Bundle extras;
    private EditText editTitle, editDescription;
    private boolean isInsert = true; // true if inserting, false if updating
    private String colorNew;
    private boolean isNight = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (((App) getApplicationContext()).isNightModeEnabled()) {
            isNight = true;
            setTheme(R.style.ActivityThemeDark);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_subject);

        editTitle = findViewById(R.id.titleEdit);
        editDescription = findViewById(R.id.descriptionEdit);
        final Button colorEdit = findViewById(R.id.colorEdit);

        extras = getIntent().getExtras();
        // https://github.com/AzeeSoft/AndroidPhotoshopColorPicker
        final ColorPickerDialog colorPickerDialog;
        if (isNight) {
            colorPickerDialog = ColorPickerDialog.createColorPickerDialog(this, ColorPickerDialog.DARK_THEME);
        } else {
            colorPickerDialog = ColorPickerDialog.createColorPickerDialog(this);
        }
        colorPickerDialog.setOnColorPickedListener(new ColorPickerDialog.OnColorPickedListener() {
            @Override
            public void onColorPicked(int color, String hexVal) {
                colorNew = hexVal;
                Log.d(LOG_TAG, "color picker dialog picked =" + hexVal);
                colorEdit.setBackgroundColor(color);
                colorEdit.setTextColor(getContrastColor(color));
            }
        });

        colorEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                colorPickerDialog.show();
            }
        });

        colorNew = "#FFc9e2e2";
        int initialColor = 0;
        // If we are passed content, fill it in for the user to edit.
        if (extras != null) {
            String title = extras.getString(TITLE, "");
            String description = extras.getString(DESCRIPTION, "");
            colorNew = extras.getString(COLOR, "");
            if (!title.isEmpty()) {
                editTitle.setText(title);
                editTitle.requestFocus();
                editDescription.setText(description);
                Log.d(LOG_TAG, "init: color = " + colorNew);
                long c = Long.decode(colorNew);
                if (colorNew.length() < 9) c += 0xFF000000;
                initialColor = (int) c;
                setTitle("Update subject");
            } else {
                setTitle("New subject");
            }
            isInsert = !extras.containsKey(ID);
        } // Otherwise start with empty fields.
        else {
            long c = Long.decode(colorNew);
            initialColor = (int) c;
            setTitle("New subject");
        }

        colorPickerDialog.setInitialColor(initialColor);
        colorEdit.setBackgroundColor(initialColor);
        colorEdit.setTextColor(getContrastColor(initialColor));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_edit_subject, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (isInsert) {
            MenuItem menuItem = menu.findItem(R.id.action_delete);
            menuItem.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.action_save:
                updateItem();
                finish();
                return true;
            // Respond to a click on the "Delete" menu option
            case R.id.action_delete:
                showDeleteConfirmationDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateItem() {
        Intent replyIntent = new Intent();
        if (TextUtils.isEmpty(editTitle.getText())) {
            setResult(RESULT_CANCELED, replyIntent);
        } else {
            String title = editTitle.getText().toString();
            String description = editDescription.getText().toString();
            replyIntent.putExtra(TITLE, title);
            replyIntent.putExtra(DESCRIPTION, description);
            replyIntent.putExtra(COLOR, colorNew);
            if (extras != null && extras.containsKey(ID)) {
                int id = extras.getInt(ID, -1);
                Log.d(LOG_TAG, "id=" + str(id));
                if (id != -1) {
                    replyIntent.putExtra(ID, id);
                }
                if (extras.containsKey(POS)) {  // must be always true at this point
                    replyIntent.putExtra(POS, extras.getInt(POS, 0));
                }
            }
            // Set the result status to indicate success.
            setResult(RESULT_OK, replyIntent);
        }
    }

    private void showDeleteConfirmationDialog() {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the positive and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_subject);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Delete" button, so delete the pet.
                deleteItem();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Cancel" button, so dismiss the dialog
                // and continue editing the pet.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void deleteItem() {
        Intent replyIntent = new Intent();
        int id = extras.getInt(ID, -1);
        replyIntent.putExtra(ID, id);
        replyIntent.putExtra(DELETE, true);
        Log.d(LOG_TAG, "deleting subject: id=" + str(id));
        setResult(RESULT_OK, replyIntent);
        finish();
    }

}
