package org.catness.classroom.rewards;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import org.catness.classroom.database.DataRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import static org.catness.classroom.utils.Utils.str;

public class RewardViewModel extends AndroidViewModel {
    private final static String LOG_TAG = RewardViewModel.class.getSimpleName();
    private final DataRepository repository;
    private final LiveData<List<Reward>> rewards;

    public RewardViewModel(@NonNull Application application) {
        super(application);
        repository = new DataRepository(application);
        rewards = repository.getAllRewards();
    }

    public LiveData<List<Reward>> getAllRewards() {
        return rewards;
    }

    public void insert(Reward reward) {
        repository.insertReward(reward);
    }

    public void deleteAll() { repository.deleteAllRewards(); }
    public void delete(int id) {
        repository.deleteReward(id);
    }

    public void update(Reward reward) {
        repository.updateReward(reward);
    }

    public String selectRandomReward() {
        if (rewards == null) {
            Log.d(LOG_TAG, "NO REWARDS");
            return "***";
        }
        else {
            // algorithm from https://medium.com/@peterkellyonline/weighted-random-selection-3ff222917eb6
            int sum = 0;
            ArrayList<Reward> rewards_tmp = new ArrayList<>();
            for (Reward r : rewards.getValue()){
                Log.d(LOG_TAG, "---------------------- reward : " + r.toString());
                if (r.amount <= 0 || r.weight <= 0) continue;
                sum += r.weight;
                rewards_tmp.add(r);
            }
            if (sum == 0) return "Whatever you want (no rewards left)";
            Collections.shuffle(rewards_tmp, new Random());
            int randomWeight = ThreadLocalRandom.current().nextInt(1, sum + 1);
            Log.d(LOG_TAG,"\nsum="+str(sum) + " randomNum=" + str(randomWeight)+"\n");
            for (Reward r : rewards_tmp) {
                randomWeight -= r.weight;
                Log.d(LOG_TAG, "---------- reward (shuffled) : " + r.toString() + " rw=" + str(randomWeight));
                if (randomWeight <= 0) {
                    repository.decrementReward(r.id);
                    return r.name;
                }
            }
            return "Whatever you want";
        }
    }


}
