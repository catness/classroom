package org.catness.classroom.rewards;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface RewardDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Reward reward);

    @Query("select * from rewards order by weight desc, name asc")
    LiveData<List<Reward>> getAllRewards();

    @Query("select * from rewards")
    List<Reward> getAllRewardsSync();

    @Query("DELETE FROM rewards")
    void deleteAll();

    @Query("delete from rewards where id=:id")
    void delete(int id);

    @Update
    void update(Reward... reward);

    @Query("update rewards set amount=amount-1 where id=:id")
    void decrement(int id);

    @Query("select count(*) from rewards")
    int getCount();

}
