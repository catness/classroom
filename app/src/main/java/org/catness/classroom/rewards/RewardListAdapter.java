package org.catness.classroom.rewards;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.catness.classroom.R;

import java.util.List;

import static org.catness.classroom.utils.Utils.getStyleColor;
import static org.catness.classroom.utils.Utils.str;

public class RewardListAdapter extends RecyclerView.Adapter<RewardListAdapter.RewardViewHolder> {
    private final static String LOG_TAG = RewardListAdapter.class.getSimpleName();
    private final LayoutInflater mInflater;
    private List<Reward> rewards;
    private static ClickListener clickListener;
    private final Context context;
    private int bgColor = 0;
    private final int rewardZero;

    public RewardListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        this.context = context;

        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.windowBackground, typedValue, true)) {
            bgColor = typedValue.data;
            Log.d(LOG_TAG, "bgColor = " + str(bgColor));
        }
        rewardZero = getStyleColor(context, "rewardZero");
    }

    @NonNull
    @Override
    public RewardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = mInflater.inflate(R.layout.reward_item, parent, false);
        return new RewardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RewardViewHolder holder, int position) {
        if (rewards != null) {
            Reward current = rewards.get(position);
            Log.d(LOG_TAG, "reward: " + current.toString());
            holder.nameView.setText(current.name);
            holder.amountAndWeightView.setText(context.getString(R.string.amount_and_weight, current.amount, current.weight));
            if (current.amount == 0) {
                holder.amountAndWeightView.setBackgroundColor(rewardZero);
            } else {
                holder.amountAndWeightView.setBackgroundColor(bgColor);
            }
        } else {
            // Covers the case of data not being ready yet.
            //holder.nameView.setText("No rewards yet");
            Log.d(LOG_TAG, "no rewards yet");
        }
    }

    public void setRewards(List<Reward> params) {
        rewards = params;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (rewards != null)
            return rewards.size();
        else return 0;
    }

    public Reward getRewardAtPosition(int position) {
        return rewards.get(position);
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
    }


    public void setOnItemClickListener(ClickListener clickListener) {
        RewardListAdapter.clickListener = clickListener;
    }

    public class RewardViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameView, amountAndWeightView;


        RewardViewHolder(@NonNull View itemView) {
            super(itemView);
            nameView = itemView.findViewById(R.id.nameView);
            amountAndWeightView = itemView.findViewById(R.id.amountAndWeightView);

            // this listener sends the clicks to the main activity where they are processed
            // at adapter.setOnItemClickListener
            // or something. anyway without this part the clicks are not processed
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onItemClick(view, getAdapterPosition());
                }
            });

        }
    }
}
