package org.catness.classroom.rewards;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import static org.catness.classroom.utils.Utils.str;

@Entity(tableName = "rewards")
public class Reward {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @NonNull
    public String name;
    public String description;
    public int weight; // to calculate the probability of getting it
    public int amount; // how many of them are in the pool

    public Reward(@NonNull String name, String description, int weight, int amount) {
        this.name = name;
        this.description = description;
        this.weight = weight;
        this.amount = amount;
    }

    @Ignore
    public Reward(int id, @NonNull String name, String description, int weight, int amount) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.weight = weight;
        this.amount = amount;
    }

    @Ignore
    public Reward() {
    }

    public String toString() {
        return "Reward: " + str(id) + " : " + name + " : " + description + " : weight=" + str(weight) + " : amount=" + str(amount);
    }
}
