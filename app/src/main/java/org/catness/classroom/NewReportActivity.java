package org.catness.classroom;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Locale;

import static org.catness.classroom.utils.Constants.COLOR;
import static org.catness.classroom.utils.Constants.COMMENT;
import static org.catness.classroom.utils.Constants.DATETIME;
import static org.catness.classroom.utils.Constants.DELETE;
import static org.catness.classroom.utils.Constants.ID;
import static org.catness.classroom.utils.Constants.LASTPOINTS;
import static org.catness.classroom.utils.Constants.POINTS;
import static org.catness.classroom.utils.Constants.SUBJECT;
import static org.catness.classroom.utils.Utils.dateToTimestamp;
import static org.catness.classroom.utils.Utils.str;
import static org.catness.classroom.utils.Utils.strDateTime;

public class NewReportActivity extends AppCompatActivity {
    private static final String LOG_TAG = NewReportActivity.class.getSimpleName();
    private String color;
    private EditText editPoints, editComment;
    private Button editDate, editTime;
    private TextView viewSubject;
    private Bundle extras;
    private boolean isInsert = true; // true if inserting, false if updating
    private boolean isNight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (((App) getApplicationContext()).isNightModeEnabled()) {
            setTheme(R.style.ActivityThemeDark);
            isNight = true;
        } else {
            isNight = false;
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_report);

        editPoints = findViewById(R.id.pointsEdit);
        editDate = findViewById(R.id.dateDate);
        editTime = findViewById(R.id.dateTime);
        editComment = findViewById(R.id.commentEdit);
        viewSubject = findViewById(R.id.subjectView);

        extras = getIntent().getExtras();
        String subject = extras.getString(SUBJECT, "");
        viewSubject.setText(subject);
        color = extras.getString(COLOR, "");
        if (extras.containsKey(POINTS)) {
            int points = extras.getInt(POINTS, 0);
            editPoints.setText(str(points));
            String comment = extras.getString(COMMENT, "");
            editComment.setText(comment);
        }
        String datetime;
        if (extras.containsKey(DATETIME)) {
            datetime = extras.getString(DATETIME, "");
        } else {
            datetime = strDateTime(); // current date / time
        }

        String[] s = datetime.split(" ");
        editDate.setText(s[0]);
        editTime.setText(s[1]);
        Log.d(LOG_TAG, "datetime: " + datetime + " date=" + s[0] + " time=" + s[1]);

        if (extras.containsKey(ID)) {
            isInsert = false;
            setTitle("Update report");
        } else {
            isInsert = true;
            setTitle("New report");
        }

        final Context context = this;

        editDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String dateStr = editDate.getText().toString();
                String[] s = dateStr.split("-");
                int mYear = Integer.parseInt(s[0]);
                int mMonth = Integer.parseInt(s[1]);
                int mDay = Integer.parseInt(s[2]);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        (isNight) ? android.app.AlertDialog.THEME_DEVICE_DEFAULT_DARK : 0,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Log.d(LOG_TAG, "picked date: " + str(year) + "-" + str(monthOfYear) + "-" + str(dayOfMonth));
                                String s = String.format(Locale.ENGLISH, "%04d-%02d-%02d", year, monthOfYear, dayOfMonth);
                                editDate.setText(s);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });


        editTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String timeStr = editTime.getText().toString();
                String[] s = timeStr.split(":");
                int mHour = Integer.parseInt(s[0]);
                int mMinute = Integer.parseInt(s[1]);
                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                        (isNight) ? android.app.AlertDialog.THEME_DEVICE_DEFAULT_DARK : 0,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                Log.d(LOG_TAG, "picked time: " + str(hourOfDay) + ":" + str(minute));
                                String s = String.format(Locale.ENGLISH, "%02d:%02d", hourOfDay, minute);
                                editTime.setText(s);
                            }
                        }, mHour, mMinute, true);
                timePickerDialog.show();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_edit_report, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        // If this is a new pet, hide the "Delete" menu item.
        if (isInsert) {
            MenuItem menuItem = menu.findItem(R.id.action_delete);
            menuItem.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.action_save:
                boolean ok = updateItem();
                if (ok) {
                    finish();
                } else {
                    Log.d(LOG_TAG, "not saving");
                    cancel();
                }
                return true;
                // Respond to a click on the "Delete" menu option
            case R.id.action_delete:
                showDeleteConfirmationDialog();
                return true;
        }
        Log.d(LOG_TAG, "end onOptionsItemSelected");
        //return super.onOptionsItemSelected(item);
        cancel();
        return true;
    }


    private void cancel() {
        Intent replyIntent = new Intent();
        setResult(RESULT_CANCELED, replyIntent);
        Log.d(LOG_TAG, "update canceled");
        finish();
    }


    private boolean updateItem() {
        Intent replyIntent = new Intent();
        if (TextUtils.isEmpty(editPoints.getText())) {
            setResult(RESULT_CANCELED, replyIntent);
        } else {
            String pointsStr = editPoints.getText().toString();
            int points;
            try {
                points = Integer.parseInt(pointsStr);
            } catch (NumberFormatException e) {
                Toast.makeText(getApplicationContext(), R.string.invalid_number, Toast.LENGTH_LONG).show();
                return false;
            }

            String datetime = editDate.getText().toString() + " " + editTime.getText().toString();
            Long timestamp = dateToTimestamp(datetime);
            if (timestamp == 0L) {
                Toast.makeText(getApplicationContext(), R.string.invalid_date, Toast.LENGTH_LONG).show();
                return false;
            }

            replyIntent.putExtra(POINTS, points);
            replyIntent.putExtra(DATETIME, timestamp);
            replyIntent.putExtra(COMMENT, editComment.getText().toString());
            replyIntent.putExtra(SUBJECT, viewSubject.getText().toString());
            replyIntent.putExtra(COLOR, color);

            if (extras != null && extras.containsKey(ID)) {
                int id = extras.getInt(ID, -1);
                Log.d(LOG_TAG, "id=" + str(id));
                if (id != -1) {
                    replyIntent.putExtra(ID, id);
                }
            }
            /* for update: we need the previous amount of points, to subtract from the level before adding the new points */
            if (extras != null && extras.containsKey(POINTS)) {
                int lastpoints = extras.getInt(POINTS, 0);
                replyIntent.putExtra(LASTPOINTS, lastpoints);
            }

            // Set the result status to indicate success.
            setResult(RESULT_OK, replyIntent);
        }
        return true;
    }

    private void showDeleteConfirmationDialog() {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the positive and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_report);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Delete" button, so delete the pet.
                deleteItem();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Cancel" button, so dismiss the dialog
                // and continue editing the pet.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /**
     * Perform the deletion of the pet in the database.
     */
    private void deleteItem() {
        Intent replyIntent = new Intent();
        int id = extras.getInt(ID, -1);
        replyIntent.putExtra(ID, id);
        replyIntent.putExtra(DELETE, true);
        Log.d(LOG_TAG, "deleting report: id=" + str(id));
        setResult(RESULT_OK, replyIntent);
        finish();
    }


}
