package org.catness.classroom;

import android.app.Application;
import android.content.SharedPreferences;
import android.support.v7.preference.PreferenceManager;

import static org.catness.classroom.utils.Constants.PREF_NIGHT;


public class App extends Application {
    public static final String LOG_TAG = App.class.getSimpleName();

    private boolean isNightModeEnabled = true;

    @Override
    public void onCreate() {
        super.onCreate();
// We load the Night Mode state here
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        isNightModeEnabled = mPrefs.getBoolean(PREF_NIGHT, false);
    }

    public boolean isNightModeEnabled() {
        return isNightModeEnabled;
    }

    public String getMyName() {
        return "Classroom";
    }

    public void setIsNightModeEnabled(boolean isNightModeEnabled) {
        this.isNightModeEnabled = isNightModeEnabled;
    }

}

