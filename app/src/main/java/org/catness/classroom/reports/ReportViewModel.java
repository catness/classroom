package org.catness.classroom.reports;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import org.catness.classroom.database.DataRepository;

import java.util.List;

public class ReportViewModel extends AndroidViewModel {
    private static final String LOG_TAG = ReportViewModel.class.getSimpleName();
    private final DataRepository repository;
    //private LiveData<List<Report>> reports;

    public ReportViewModel(@NonNull Application application) {
        super(application);
        repository = new DataRepository(application);
        //reports = repository.getAllReports();
    }

    public LiveData<List<Report>> getAllReports(Integer limit) {
        return repository.getAllReports(limit);
    }

    public void insert(Report report, int maxpoints) {
        repository.insertReport(report, maxpoints);
    }

    public void delete(int id, int maxpoints) {
        repository.deleteReport(id, maxpoints);
    }

    public void deleteAll() {
        repository.deleteAllReports();
    }

    public void update(Report report, int points, int maxpoints) {
        repository.updateReport(report, points, maxpoints);
    }

    public int getPointsDay(Long start, Long end) {
        return repository.getPointsDay(start, end);
    }

    public LiveData<List<Report>> getAllReportsDay(Long start, Long end) {
        return repository.getAllReportsDay(start, end);
    }


}
