package org.catness.classroom.reports;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface ReportDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Report report);

    @Query("select * from reports order by datetime desc limit :limit")
    LiveData<List<Report>> getAllReports(Integer limit);

    @Query("select * from reports")
    LiveData<List<Report>> getAllReportsNoLimit();

    @Query("select * from reports")
    List<Report> getAllReportsSync();

    @Query("select * from reports where datetime>=:start and datetime<=:end order by datetime desc")
    LiveData<List<Report>> getAllReportsDay(Long start, Long end);

    @Query("select * from reports where id=:id")
    Report getReportSimple(int id);

    @Query("delete from reports where id=:id")
    void delete(int id);

    @Query("DELETE FROM reports")
    void deleteAll();

    @Update
    void update(Report... report);

    @Query("select sum(points) from reports where datetime>=:start and datetime<=:end")
    int getPointsDay(Long start, Long end);

    @Query("select count(*) from reports")
    int getCount();
}
