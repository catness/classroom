package org.catness.classroom.reports;


import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.catness.classroom.R;
import org.catness.classroom.utils.Utils;

import java.util.List;

import static org.catness.classroom.utils.Utils.getContrastColor;
import static org.catness.classroom.utils.Utils.getStyleColor;
import static org.catness.classroom.utils.Utils.str;
import static org.catness.classroom.utils.Utils.strDateOnly;
import static org.catness.classroom.utils.Utils.strDateTimeShort;

public class ReportListAdapter extends RecyclerView.Adapter<ReportListAdapter.ReportViewHolder> {
    private static final String LOG_TAG = ReportListAdapter.class.getSimpleName();
    private final LayoutInflater mInflater;
    private List<Report> reports; // Cached copy of reports
    private static ClickListener clickListener;
    private final Context context;

    public ReportListAdapter(Context context) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }


    @NonNull
    @Override
    public ReportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.report_item, parent, false);
        return new ReportViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportViewHolder holder, int position) {
        if (reports != null) {
            Report current = reports.get(position);
            if (current == null) {
                //Log.d(LOG_TAG, "current report is null : should not happen. pos=" + str(position));
                return;
            }
            //Log.d(LOG_TAG, current.toString());
            holder.subjectView.setText(current.subject);
            holder.pointsView.setText(str(current.points));
            String date = strDateOnly(current.datetime);
            holder.dateView.setText(strDateTimeShort(current.datetime));
            int bg, fg;
            try {
                bg = Color.parseColor(current.color);
                holder.itemView.setBackgroundColor(bg);
                fg = getContrastColor(bg);
                holder.subjectView.setTextColor(fg);
                holder.pointsView.setTextColor(fg);
                holder.dateView.setTextColor(fg);
            } catch (Exception e) {
                Log.d(LOG_TAG, "invalid color");
            }
            try {
                Report previous = reports.get(position - 1);
                String datePrev = strDateOnly(previous.datetime);
                if (!date.equals(datePrev)) {
                    // Log.d(LOG_TAG, "~~~~~previous report: " + previous.toString());
                    showDateTitle(holder, date);
                } else {
                    holder.dateHeader.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                // the 1st report: we only show the header if the date is different from today
                String dateToday = Utils.strDateToday();
                if (dateToday.equals(date)) {
                    holder.dateHeader.setVisibility(View.GONE);
                } else {
                    showDateTitle(holder, date);
                }
            }
        }
    }

    private void showDateTitle(ReportViewHolder holder, String date) {
        holder.dateHeader.setVisibility(View.VISIBLE);
        holder.dateHeader.setText(date);
        holder.dateHeader.setBackgroundColor(getStyleColor(context, "colorDateBG"));
        holder.dateHeader.setTextColor(getStyleColor(context, "colorDateFG"));
    }

    public void setReports(List<Report> params) {
        reports = params;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // subjects has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (reports != null)
            return reports.size();
        else return 0;
    }

    public Report getReportAtPosition(int position) {
        return reports.get(position);
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
    }


    public void setOnItemClickListener(ClickListener clickListener) {
        ReportListAdapter.clickListener = clickListener;
    }


    class ReportViewHolder extends RecyclerView.ViewHolder {
        private final TextView subjectView, pointsView, dateView, dateHeader;

        private ReportViewHolder(View itemView) {
            super(itemView);
            subjectView = itemView.findViewById(R.id.subjectView);
            pointsView = itemView.findViewById(R.id.pointsView);
            dateView = itemView.findViewById(R.id.dateView);
            dateHeader = itemView.findViewById(R.id.dateHeader);

            // this listener sends the clicks to the main activity where they are processed
            // at adapter.setOnItemClickListener
            // or something. anyway without this part the clicks are not processed
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onItemClick(view, getAdapterPosition());
                }
            });

        }
    }

}
