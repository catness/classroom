package org.catness.classroom.reports;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import static org.catness.classroom.utils.Utils.str;
import static org.catness.classroom.utils.Utils.strDateTime;

@Entity(tableName = "reports")
public class Report {
    @PrimaryKey(autoGenerate = true)
    public int id;

    public String subject;
    public String color;
    public long datetime;
    public int points;
    public String comment;

    public Report(String subject, String color, long datetime, int points, String comment) {
        this.subject = subject;
        this.color = color;
        this.datetime = datetime;
        this.points = points;
        this.comment = comment;
    }

    @Ignore
    public Report(int id, String subject, String color, long datetime, int points, String comment) {
        this.id = id;
        this.subject = subject;
        this.color = color;
        this.datetime = datetime;
        this.points = points;
        this.comment = comment;
    }

    @Ignore
    public Report() {
    }

    public String toString() {
        return "Report: " + str(id) + " : " + subject + " : " + str(points) + " : " + str(datetime) + " : " + strDateTime(datetime);
    }
}
