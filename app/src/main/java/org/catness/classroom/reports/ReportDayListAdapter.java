package org.catness.classroom.reports;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.catness.classroom.R;
import org.catness.classroom.utils.ItemTouchHelperAdapter;

import java.util.List;

import static org.catness.classroom.utils.Constants.SWIPE_LEFT;
import static org.catness.classroom.utils.Utils.getContrastColor;
import static org.catness.classroom.utils.Utils.str;
import static org.catness.classroom.utils.Utils.strDateTimeShort;

public class ReportDayListAdapter extends RecyclerView.Adapter<ReportDayListAdapter.ReportDayViewHolder> implements ItemTouchHelperAdapter {
    private final static String LOG_TAG = ReportListAdapter.class.getSimpleName();
    private final LayoutInflater mInflater;
    private List<Report> reports;
    private static ClickListener clickListener;
    private static SwipeListener swipeListener;

    public ReportDayListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ReportDayViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = mInflater.inflate(R.layout.report_day_item, parent, false);
        Log.d(LOG_TAG, "onCreateViewHolder");
        return new ReportDayViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportDayViewHolder holder, int position) {
        if (reports != null) {
            Report current = reports.get(position);
            Log.d(LOG_TAG, current.toString());
            holder.subjectView.setText(current.subject);
            holder.pointsView.setText(str(current.points));
            holder.dateView.setText(strDateTimeShort(current.datetime));
            boolean invalidColor = false;
            int bg, fg = 0;
            try {
                bg = Color.parseColor(current.color);
                holder.itemView.setBackgroundColor(bg);
                fg = getContrastColor(bg);
                holder.subjectView.setTextColor(fg);
                holder.pointsView.setTextColor(fg);
                holder.dateView.setTextColor(fg);
            } catch (Exception e) {
                Log.d(LOG_TAG, "can't get color");
                invalidColor = true;
            }
            if (current.comment.isEmpty()) {
                holder.commentView.setVisibility(View.GONE);
            } else {
                holder.commentView.setVisibility(View.VISIBLE);
                holder.commentView.setText(current.comment);
                if (!invalidColor) holder.commentView.setTextColor(fg);
            }
        } else {
            // Covers the case of data not being ready yet.
            Log.d(LOG_TAG, "No data yet!");
            holder.subjectView.setText(R.string.no_data_yet);
            holder.itemView.setBackgroundColor(Color.BLACK);
            holder.subjectView.setTextColor(Color.WHITE);
        }
    }

    public void setReports(List<Report> params) {
        reports = params;
        Log.d(LOG_TAG, "setReports: count=" + reports.size());
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // subjects has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (reports != null)
            return reports.size();
        else return 0;
    }

    public Report getReportAtPosition(int position) {
        return reports.get(position);
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {

    }

    @Override
    public void onItemDismiss(int direction) {
        if (direction == SWIPE_LEFT) {
            Log.d(LOG_TAG, "swipe left " + str(direction));
        } else {
            Log.d(LOG_TAG, "swipe right " + str(direction));
        }
        swipeListener.onSwipe(direction);
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
    }

    public interface SwipeListener {
        void onSwipe(int direction);
    }


    public void setOnItemClickListener(ClickListener clickListener) {
        ReportDayListAdapter.clickListener = clickListener;
    }


    public void setOnSwipeClickListener(SwipeListener swipeListener) {
        ReportDayListAdapter.swipeListener = swipeListener;
    }

    public class ReportDayViewHolder extends RecyclerView.ViewHolder {
        private final TextView subjectView, pointsView, dateView, commentView;

        public ReportDayViewHolder(@NonNull View itemView) {
            super(itemView);
            subjectView = itemView.findViewById(R.id.subjectView);
            pointsView = itemView.findViewById(R.id.pointsView);
            dateView = itemView.findViewById(R.id.dateView);
            commentView = itemView.findViewById(R.id.commentView);

            // this listener sends the clicks to the main activity where they are processed
            // at adapter.setOnItemClickListener
            // or something. anyway without this part the clicks are not processed
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onItemClick(view, getAdapterPosition());
                }
            });
        }
    }
}
