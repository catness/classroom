package org.catness.classroom;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import static org.catness.classroom.utils.Constants.AMOUNT;
import static org.catness.classroom.utils.Constants.DELETE;
import static org.catness.classroom.utils.Constants.DESCRIPTION;
import static org.catness.classroom.utils.Constants.ID;
import static org.catness.classroom.utils.Constants.NAME;
import static org.catness.classroom.utils.Constants.WEIGHT;
import static org.catness.classroom.utils.Utils.str;

public class NewRewardActivity extends AppCompatActivity {
    private static final String LOG_TAG = NewRewardActivity.class.getSimpleName();
    private EditText editName, editDescription, editWeight, editAmount;
    private Bundle extras;
    private boolean isInsert = true; // true if inserting, false if updating

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (((App) getApplicationContext()).isNightModeEnabled())
            setTheme(R.style.ActivityThemeDark);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_reward);

        editName = findViewById(R.id.titleEdit);
        editDescription = findViewById(R.id.descriptionEdit);
        editAmount = findViewById(R.id.amountEdit);
        editWeight = findViewById(R.id.weightEdit);

        extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(ID)) {
            isInsert = false;
            int weight = extras.getInt(WEIGHT, 1);
            editWeight.setText(str(weight));
            int amount = extras.getInt(AMOUNT, 1);
            editAmount.setText(str(amount));

            String name = extras.getString(NAME, "");
            editName.setText(name);
            String description = extras.getString(DESCRIPTION, "");
            editDescription.setText(description);
            setTitle("Update reward");
        } else {
            setTitle("New reward");
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_edit_reward, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        // If this is a new pet, hide the "Delete" menu item.
        if (isInsert) {
            MenuItem menuItem = menu.findItem(R.id.action_delete);
            menuItem.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.action_save:
                boolean ok = updateItem();
                if (ok) {
                    finish();
                    return true;
                } else return false;
                // Respond to a click on the "Delete" menu option
            case R.id.action_delete:
                showDeleteConfirmationDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean updateItem() {
        Intent replyIntent = new Intent();
        if (TextUtils.isEmpty(editName.getText())) {
            setResult(RESULT_CANCELED, replyIntent);
        } else {
            String amountStr = editAmount.getText().toString();
            int amount;
            try {
                amount = Integer.parseInt(amountStr);
            } catch (NumberFormatException e) {
                Toast.makeText(getApplicationContext(), R.string.invalid_number, Toast.LENGTH_LONG).show();
                return false;
            }

            String weightStr = editWeight.getText().toString();
            int weight;
            try {
                weight = Integer.parseInt(weightStr);
            } catch (NumberFormatException e) {
                Toast.makeText(getApplicationContext(), R.string.invalid_number, Toast.LENGTH_LONG).show();
                return false;
            }

            if (amount < 0 || weight < 0) {
                Toast.makeText(getApplicationContext(), R.string.invalid_number_must_be_positive, Toast.LENGTH_LONG).show();
                return false;
            }

            replyIntent.putExtra(AMOUNT, amount);
            replyIntent.putExtra(WEIGHT, weight);
            replyIntent.putExtra(NAME, editName.getText().toString());
            replyIntent.putExtra(DESCRIPTION, editDescription.getText().toString());

            if (extras != null && extras.containsKey(ID)) {
                int id = extras.getInt(ID, -1);
                Log.d(LOG_TAG, "EDIT reward: id=" + str(id));
                if (id != -1) {
                    replyIntent.putExtra(ID, id);
                }
            }

            // Set the result status to indicate success.
            setResult(RESULT_OK, replyIntent);
        }
        return true;
    }

    private void showDeleteConfirmationDialog() {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the positive and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_reward);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Delete" button, so delete the pet.
                deleteItem();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Cancel" button, so dismiss the dialog
                // and continue editing the pet.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /**
     * Perform the deletion of the pet in the database.
     */
    private void deleteItem() {
        Intent replyIntent = new Intent();
        int id = extras.getInt(ID, -1);
        replyIntent.putExtra(ID, id);
        replyIntent.putExtra(DELETE, true);
        Log.d(LOG_TAG, "deleting reward id=" + str(id));
        setResult(RESULT_OK, replyIntent);
        finish();
    }


}
