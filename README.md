# About the project

**Classroom** is a gamification app which represents real-life habits and activities as classes in a magical school (or actually any other kind of a sci-fi/fantasy educational establishment). Give yourself points for classes (as many as you believe you deserve! negative points are also ok), level up and get a random real-life reward from the list of rewards you specify in advance. (The game does not buy you the actual reward ;) Rather, it gives you the permission to buy it for yourself.)

**Features**:

* Choose a color for every class.

* Add a short note to every completed activity. (The notes can be seen on the calendar page, upon clicking on a specific date.)

* See the calendar for the overview of how well you did in the past.

* Log the activities for past dates.

* Assign different weights for rewards to increase the probability of getting a specific reward.

* Two skins: light and dark.

* User-created themes for levels. The app comes with the Tarot theme by default, and another two (animals and countries) are available as zip files in the *resources* directory.

* Customizable app header. So you can call your school Hogwarts, Starfleet Academy, Miskatonic University, The Jedi Praxeum or whatever you like. (You can also create a matching level theme for it.)

* Your data is only stored locally! Export the database as a csv file, and import if you need to transfer it to another phone. 


The apk can be downloaded [here](https://bitbucket.org/catness/classroom/downloads/). It should work with Android 5.0 and above.

Free resources used in the game: [resources.md](docs/resources.md)

Screenshots: [screenshots directory](screenshots/)

**How to create and upload themes**:

*[TODO]* *Contact me if you need the explanation. Now it's somewhere in the sources. First, enable the themes in the development options.*

**How to export and import the database**:

*[TODO]* *Contact me if you need the explanation. Now it's somewhere in the sources. First, enable the export/import in the development options.*
